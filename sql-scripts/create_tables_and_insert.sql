CREATE TABLE assignments
(
    id BIGINT PRIMARY KEY NOT NULL,
    user_id BIGINT NOT NULL,
    card_id BIGINT NOT NULL,
    assign_date TIMESTAMP,
    CONSTRAINT card_assign_history_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT fk8iy60sfbpptmtm10f4al96qvq FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT card_assign_history_responsibility_cards_id_fk FOREIGN KEY (card_id) REFERENCES responsibility_cards (id),
    CONSTRAINT fk3f00pti9idrc21cnauumcic5w FOREIGN KEY (card_id) REFERENCES responsibility_cards (id)
);
CREATE TABLE benefit_types
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(64) NOT NULL
);
CREATE UNIQUE INDEX benefit_types_name_key ON benefit_types (name);
CREATE TABLE career_levels
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(64) NOT NULL,
    points_to_pass BIGINT DEFAULT 1000000
);
CREATE UNIQUE INDEX career_levels_name_key ON career_levels (name);
CREATE TABLE career_levels_benefits
(
    career_level_id BIGINT NOT NULL,
    benefit_type_id INTEGER NOT NULL,
    quantity INTEGER DEFAULT 1 NOT NULL,
    CONSTRAINT career_levels_benefits_pkey PRIMARY KEY (career_level_id, benefit_type_id),
    CONSTRAINT fkn5g97vm2pw15r1hivlxh3a5ge FOREIGN KEY (career_level_id) REFERENCES career_levels (id),
    CONSTRAINT fk97w1nju0fpm8ngr4a7km9l1ha FOREIGN KEY (benefit_type_id) REFERENCES benefit_types (id)
);
CREATE TABLE career_levels_todos
(
    career_level_id BIGINT NOT NULL,
    todo_type_id INTEGER NOT NULL,
    points INTEGER DEFAULT 0 NOT NULL,
    mandatory INTEGER DEFAULT 0 NOT NULL,
    CONSTRAINT career_levels_todos_pkey PRIMARY KEY (career_level_id, todo_type_id),
    CONSTRAINT fk1mq1cnfg0u6pwvtfbo54okhs7 FOREIGN KEY (career_level_id) REFERENCES career_levels (id),
    CONSTRAINT fkc6isbapaf34bovy5mprhp9rrh FOREIGN KEY (todo_type_id) REFERENCES todo_types (id)
);
CREATE TABLE event_attend
(
    id INTEGER PRIMARY KEY NOT NULL,
    event_id BIGINT,
    user_id BIGINT,
    CONSTRAINT fkn66c1uuss9a187n0de5j02rsp FOREIGN KEY (event_id) REFERENCES events (id),
    CONSTRAINT fkpy4m3polhyxk1demnwl167fgk FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE TABLE event_comments
(
    id INTEGER PRIMARY KEY NOT NULL,
    text VARCHAR(255),
    event_id BIGINT,
    user_id BIGINT,
    date TIMESTAMP DEFAULT now() NOT NULL
);
CREATE TABLE event_seen
(
    id INTEGER PRIMARY KEY NOT NULL,
    event_id INTEGER,
    user_id INTEGER,
    CONSTRAINT fkav9u1fre5r0kh7s8c66oga4pg FOREIGN KEY (event_id) REFERENCES events (id),
    CONSTRAINT fkrdd4bc63emk4kt6p07id64rgw FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE TABLE events
(
    id INTEGER PRIMARY KEY NOT NULL,
    name VARCHAR(255) DEFAULT 'Event'::character varying,
    description VARCHAR(255) DEFAULT 'Attend it!'::character varying,
    active INTEGER DEFAULT 1 NOT NULL,
    date TIMESTAMP DEFAULT now(),
    place VARCHAR(255) DEFAULT 'Chișinău, Moldova'::character varying,
    author_id BIGINT,
    author INTEGER DEFAULT 1,
    CONSTRAINT fkqb2sh2nyr3kdqgutaqll5cfpr FOREIGN KEY (author_id) REFERENCES users (id),
    CONSTRAINT fk7gaujeodsxymr1fs9h0uy4wtq FOREIGN KEY (author_id) REFERENCES users (id)
);
CREATE TABLE groups
(
    id BIGINT PRIMARY KEY NOT NULL,
    role VARCHAR(64) NOT NULL
);
CREATE UNIQUE INDEX groups_role_key ON groups (role);
CREATE TABLE notification
(
    id BIGINT PRIMARY KEY NOT NULL,
    user_id BIGINT NOT NULL,
    type INTEGER DEFAULT 1,
    req_card_id INTEGER,
    event_id INTEGER,
    CONSTRAINT fknk4ftb5am9ubmkv1661h15ds9 FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT fk64qor2dn1apbjf7rgnkw9qm89 FOREIGN KEY (req_card_id) REFERENCES responsibility_cards (id),
    CONSTRAINT fk62mnpgloeus6qb2j7cby0gid5 FOREIGN KEY (event_id) REFERENCES events (id)
);
CREATE UNIQUE INDEX promote_notification_id_uindex ON notification (id);
CREATE TABLE responsibility_cards
(
    id INTEGER PRIMARY KEY NOT NULL,
    image_path VARCHAR(255),
    description TEXT NOT NULL,
    expected_result TEXT,
    type VARCHAR(255) NOT NULL,
    user_id INTEGER NOT NULL,
    CONSTRAINT responsibility_cards_users_id_fk FOREIGN KEY ("?") REFERENCES users (id),
    CONSTRAINT fklvrmahoxumlls3qllxctnlsl7 FOREIGN KEY ("?") REFERENCES users (id)
);
CREATE TABLE todo_types
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(64) NOT NULL
);
CREATE UNIQUE INDEX todo_types_name_key ON todo_types (name);
CREATE TABLE todos
(
    user_id BIGINT NOT NULL,
    career_level_id INTEGER NOT NULL,
    todo_id VARCHAR(64) NOT NULL,
    todo_type_id INTEGER NOT NULL,
    date_of_completion TIMESTAMP DEFAULT now() NOT NULL,
    description VARCHAR(256) DEFAULT NULL::character varying,
    CONSTRAINT todos_pkey PRIMARY KEY (user_id, career_level_id, todo_id),
    CONSTRAINT fk9605g76a1dggbvs18f2r80gvu FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT fka43ince4ply4ch04kv8jivpvy FOREIGN KEY (career_level_id) REFERENCES career_levels (id)
);
CREATE TABLE users
(
    id BIGINT PRIMARY KEY NOT NULL,
    last_name VARCHAR(64) NOT NULL,
    first_name VARCHAR(64) NOT NULL,
    username VARCHAR(64) NOT NULL,
    password_hash VARCHAR(64) NOT NULL,
    email VARCHAR(64) NOT NULL,
    career_level_id INTEGER NOT NULL,
    group_id INTEGER NOT NULL,
    CONSTRAINT fk6b2cs8xemrkie24tsbkql5s44 FOREIGN KEY (career_level_id) REFERENCES career_levels (id),
    CONSTRAINT fkemfuglprp85bh5xwhfm898ysc FOREIGN KEY (group_id) REFERENCES groups (id)
);
CREATE UNIQUE INDEX users_username_email_key ON users (username, email);

