--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

-- Started on 2017-07-10 11:28:11

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2303 (class 1262 OID 16397)
-- Name: career_levels; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE career_levels WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';


ALTER DATABASE career_levels OWNER TO postgres;

\connect career_levels

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2305 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 25090)
-- Name: assignments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE assignments (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    card_id bigint NOT NULL,
    assign_date timestamp without time zone
);


ALTER TABLE assignments OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 24840)
-- Name: benefit_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE benefit_types (
    id bigint NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE benefit_types OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 24875)
-- Name: career_levels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE career_levels (
    id bigint NOT NULL,
    name character varying(64) NOT NULL,
    points_to_pass bigint DEFAULT 1000000
);


ALTER TABLE career_levels OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 24834)
-- Name: career_levels_benefits; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE career_levels_benefits (
    career_level_id bigint NOT NULL,
    benefit_type_id integer NOT NULL,
    quantity integer DEFAULT 1 NOT NULL
);


ALTER TABLE career_levels_benefits OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 24854)
-- Name: career_levels_todos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE career_levels_todos (
    career_level_id bigint NOT NULL,
    todo_type_id integer NOT NULL,
    points integer DEFAULT 0 NOT NULL,
    mandatory integer DEFAULT 0 NOT NULL
);


ALTER TABLE career_levels_todos OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 25168)
-- Name: event_attend; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE event_attend (
    id integer NOT NULL,
    event_id bigint,
    user_id bigint
);


ALTER TABLE event_attend OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 25166)
-- Name: event_attend_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE event_attend_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_attend_id_seq OWNER TO postgres;

--
-- TOC entry 2306 (class 0 OID 0)
-- Dependencies: 204
-- Name: event_attend_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE event_attend_id_seq OWNED BY event_attend.id;


--
-- TOC entry 203 (class 1259 OID 25154)
-- Name: event_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE event_comments (
    id integer NOT NULL,
    text character varying(255),
    event_id bigint,
    user_id bigint,
    date timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE event_comments OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 25152)
-- Name: event_commentd_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE event_commentd_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_commentd_id_seq OWNER TO postgres;

--
-- TOC entry 2307 (class 0 OID 0)
-- Dependencies: 202
-- Name: event_commentd_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE event_commentd_id_seq OWNED BY event_comments.id;


--
-- TOC entry 207 (class 1259 OID 25202)
-- Name: event_seen; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE event_seen (
    id integer NOT NULL,
    event_id integer,
    user_id integer
);


ALTER TABLE event_seen OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 25200)
-- Name: event_seen_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE event_seen_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE event_seen_id_seq OWNER TO postgres;

--
-- TOC entry 2308 (class 0 OID 0)
-- Dependencies: 206
-- Name: event_seen_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE event_seen_id_seq OWNED BY event_seen.id;


--
-- TOC entry 201 (class 1259 OID 25137)
-- Name: events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE events (
    id integer NOT NULL,
    name character varying(255) DEFAULT 'Event'::character varying,
    description character varying(255) DEFAULT 'Attend it!'::character varying,
    active integer DEFAULT 1 NOT NULL,
    date timestamp without time zone DEFAULT now(),
    place character varying(255) DEFAULT 'Chișinău, Moldova'::character varying,
    author_id bigint,
    author integer DEFAULT 1
);


ALTER TABLE events OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 25135)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE events_id_seq OWNER TO postgres;

--
-- TOC entry 2309 (class 0 OID 0)
-- Dependencies: 200
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- TOC entry 191 (class 1259 OID 24861)
-- Name: groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE groups (
    id bigint NOT NULL,
    role character varying(64) NOT NULL
);


ALTER TABLE groups OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16934)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 24954)
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE notification (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    type integer DEFAULT 1,
    req_card_id integer,
    event_id integer
);


ALTER TABLE notification OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 25132)
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE notification_id_seq OWNER TO postgres;

--
-- TOC entry 2310 (class 0 OID 0)
-- Dependencies: 199
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE notification_id_seq OWNED BY notification.id;


--
-- TOC entry 195 (class 1259 OID 24976)
-- Name: promote_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE promote_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE promote_notification_id_seq OWNER TO postgres;

--
-- TOC entry 2311 (class 0 OID 0)
-- Dependencies: 195
-- Name: promote_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE promote_notification_id_seq OWNED BY notification.id;


--
-- TOC entry 196 (class 1259 OID 25003)
-- Name: responsibility_cards; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE responsibility_cards (
    id integer NOT NULL,
    image_path character varying(255),
    description text NOT NULL,
    expected_result text,
    type character varying(255) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE responsibility_cards OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 25062)
-- Name: responsibility_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE responsibility_cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE responsibility_cards_id_seq OWNER TO postgres;

--
-- TOC entry 2312 (class 0 OID 0)
-- Dependencies: 197
-- Name: responsibility_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE responsibility_cards_id_seq OWNED BY responsibility_cards.id;


--
-- TOC entry 186 (class 1259 OID 24747)
-- Name: todo_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE todo_types (
    id bigint NOT NULL,
    name character varying(64) NOT NULL
);


ALTER TABLE todo_types OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 24847)
-- Name: todos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE todos (
    user_id bigint NOT NULL,
    career_level_id integer NOT NULL,
    todo_id character varying(64) NOT NULL,
    todo_type_id integer NOT NULL,
    date_of_completion timestamp without time zone DEFAULT now() NOT NULL,
    description character varying(256) DEFAULT NULL::character varying
);


ALTER TABLE todos OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 24868)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id bigint NOT NULL,
    last_name character varying(64) NOT NULL,
    first_name character varying(64) NOT NULL,
    username character varying(64) NOT NULL,
    password_hash character varying(64) NOT NULL,
    email character varying(64) NOT NULL,
    career_level_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE users OWNER TO postgres;

--
-- TOC entry 2091 (class 2604 OID 25171)
-- Name: event_attend id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_attend ALTER COLUMN id SET DEFAULT nextval('event_attend_id_seq'::regclass);


--
-- TOC entry 2089 (class 2604 OID 25157)
-- Name: event_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_comments ALTER COLUMN id SET DEFAULT nextval('event_commentd_id_seq'::regclass);


--
-- TOC entry 2092 (class 2604 OID 25205)
-- Name: event_seen id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_seen ALTER COLUMN id SET DEFAULT nextval('event_seen_id_seq'::regclass);


--
-- TOC entry 2082 (class 2604 OID 25140)
-- Name: events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- TOC entry 2080 (class 2604 OID 25134)
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification ALTER COLUMN id SET DEFAULT nextval('notification_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 25064)
-- Name: responsibility_cards id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY responsibility_cards ALTER COLUMN id SET DEFAULT nextval('responsibility_cards_id_seq'::regclass);


--
-- TOC entry 2289 (class 0 OID 25090)
-- Dependencies: 198
-- Data for Name: assignments; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO assignments VALUES (69, 10, 42, '2017-07-05 10:34:26.501');
INSERT INTO assignments VALUES (72, 5, 43, '2017-07-05 10:34:56.642');
INSERT INTO assignments VALUES (76, 170, 42, '2017-07-05 11:07:19.439');
INSERT INTO assignments VALUES (79, 9, 78, '2017-07-05 11:11:51.592');
INSERT INTO assignments VALUES (80, 1, 78, '2017-07-05 11:12:02.786');
INSERT INTO assignments VALUES (81, 3, 78, '2017-07-05 11:12:34.985');
INSERT INTO assignments VALUES (83, 160, 82, '2017-07-05 11:36:52.273');
INSERT INTO assignments VALUES (85, 270, 84, '2017-07-05 11:42:26.46');
INSERT INTO assignments VALUES (86, 4, 43, '2017-07-05 11:44:27.031');
INSERT INTO assignments VALUES (125, 2, 111, '2017-07-06 10:33:03.533');
INSERT INTO assignments VALUES (126, 2, 94, '2017-07-06 10:33:20.181');
INSERT INTO assignments VALUES (127, 160, 102, '2017-07-06 10:33:22.822');
INSERT INTO assignments VALUES (128, 9, 92, '2017-07-06 10:34:18.43');
INSERT INTO assignments VALUES (129, 160, 111, '2017-07-06 10:34:23.203');
INSERT INTO assignments VALUES (130, 2, 102, '2017-07-06 10:34:25.853');
INSERT INTO assignments VALUES (131, 160, 102, '2017-07-06 10:34:27.826');
INSERT INTO assignments VALUES (132, 160, 94, '2017-07-06 10:34:29.052');
INSERT INTO assignments VALUES (133, 160, 103, '2017-07-06 10:41:08.508');
INSERT INTO assignments VALUES (134, 160, 95, '2017-07-06 10:41:11.163');
INSERT INTO assignments VALUES (135, 4, 82, '2017-07-06 10:41:22.211');
INSERT INTO assignments VALUES (136, 2, 96, '2017-07-06 10:45:30.383');


--
-- TOC entry 2279 (class 0 OID 24840)
-- Dependencies: 188
-- Data for Name: benefit_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO benefit_types VALUES (2, 'Fixed Schedule (9:00-18:00)');
INSERT INTO benefit_types VALUES (3, 'Teambuildings access');
INSERT INTO benefit_types VALUES (4, 'Table + chair');
INSERT INTO benefit_types VALUES (5, 'Lunch 50%');
INSERT INTO benefit_types VALUES (6, 'Fixed salary');
INSERT INTO benefit_types VALUES (7, 'Laptop');
INSERT INTO benefit_types VALUES (8, 'Library access');
INSERT INTO benefit_types VALUES (9, 'English classes');
INSERT INTO benefit_types VALUES (10, 'Flexible schedule1 (9:00-18:00)+/-30 min');
INSERT INTO benefit_types VALUES (11, 'New equipment priority 3');
INSERT INTO benefit_types VALUES (12, '(EURO) Personal budget');
INSERT INTO benefit_types VALUES (13, '(MDL) Monthly for sports');
INSERT INTO benefit_types VALUES (14, 'Private health insurance');
INSERT INTO benefit_types VALUES (15, 'Self-development club (1.25h per week)');
INSERT INTO benefit_types VALUES (16, 'Suggest books per year');
INSERT INTO benefit_types VALUES (17, 'OCA Java');
INSERT INTO benefit_types VALUES (18, 'Visit to NL');
INSERT INTO benefit_types VALUES (19, 'Flexible schedule2 (9:00-18:00)+/-1h');
INSERT INTO benefit_types VALUES (20, 'New equipment priority 2');
INSERT INTO benefit_types VALUES (21, 'Days per month work from home');
INSERT INTO benefit_types VALUES (22, '(EURO) credit');
INSERT INTO benefit_types VALUES (23, 'Flexible schedule3 (40h per week)');
INSERT INTO benefit_types VALUES (24, 'New equipment priority 1');
INSERT INTO benefit_types VALUES (1, 'Lunch 100%');


--
-- TOC entry 2284 (class 0 OID 24875)
-- Dependencies: 193
-- Data for Name: career_levels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO career_levels VALUES (1, 'Internship', 1000000);
INSERT INTO career_levels VALUES (2, 'Baby on board', 50);
INSERT INTO career_levels VALUES (3, 'First steps', 100);
INSERT INTO career_levels VALUES (4, 'You are good!', 150);
INSERT INTO career_levels VALUES (5, 'You rock!', 150);


--
-- TOC entry 2278 (class 0 OID 24834)
-- Dependencies: 187
-- Data for Name: career_levels_benefits; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO career_levels_benefits VALUES (1, 3, 1);
INSERT INTO career_levels_benefits VALUES (1, 4, 1);
INSERT INTO career_levels_benefits VALUES (2, 6, 1);
INSERT INTO career_levels_benefits VALUES (2, 7, 1);
INSERT INTO career_levels_benefits VALUES (2, 8, 1);
INSERT INTO career_levels_benefits VALUES (2, 3, 1);
INSERT INTO career_levels_benefits VALUES (2, 9, 1);
INSERT INTO career_levels_benefits VALUES (3, 5, 1);
INSERT INTO career_levels_benefits VALUES (3, 6, 1);
INSERT INTO career_levels_benefits VALUES (3, 10, 1);
INSERT INTO career_levels_benefits VALUES (3, 7, 1);
INSERT INTO career_levels_benefits VALUES (3, 8, 1);
INSERT INTO career_levels_benefits VALUES (3, 3, 1);
INSERT INTO career_levels_benefits VALUES (3, 9, 1);
INSERT INTO career_levels_benefits VALUES (3, 11, 1);
INSERT INTO career_levels_benefits VALUES (3, 12, 150);
INSERT INTO career_levels_benefits VALUES (3, 13, 200);
INSERT INTO career_levels_benefits VALUES (3, 14, 1);
INSERT INTO career_levels_benefits VALUES (3, 15, 1);
INSERT INTO career_levels_benefits VALUES (3, 16, 1);
INSERT INTO career_levels_benefits VALUES (3, 17, 1);
INSERT INTO career_levels_benefits VALUES (3, 18, 1);
INSERT INTO career_levels_benefits VALUES (4, 5, 1);
INSERT INTO career_levels_benefits VALUES (4, 6, 1);
INSERT INTO career_levels_benefits VALUES (4, 19, 1);
INSERT INTO career_levels_benefits VALUES (4, 7, 1);
INSERT INTO career_levels_benefits VALUES (4, 8, 1);
INSERT INTO career_levels_benefits VALUES (4, 3, 1);
INSERT INTO career_levels_benefits VALUES (4, 9, 1);
INSERT INTO career_levels_benefits VALUES (4, 20, 1);
INSERT INTO career_levels_benefits VALUES (4, 12, 300);
INSERT INTO career_levels_benefits VALUES (4, 13, 300);
INSERT INTO career_levels_benefits VALUES (4, 14, 1);
INSERT INTO career_levels_benefits VALUES (4, 15, 1);
INSERT INTO career_levels_benefits VALUES (4, 16, 3);
INSERT INTO career_levels_benefits VALUES (4, 21, 1);
INSERT INTO career_levels_benefits VALUES (4, 22, 8000);
INSERT INTO career_levels_benefits VALUES (5, 5, 1);
INSERT INTO career_levels_benefits VALUES (5, 6, 1);
INSERT INTO career_levels_benefits VALUES (5, 23, 1);
INSERT INTO career_levels_benefits VALUES (5, 7, 1);
INSERT INTO career_levels_benefits VALUES (5, 8, 1);
INSERT INTO career_levels_benefits VALUES (5, 3, 1);
INSERT INTO career_levels_benefits VALUES (5, 9, 1);
INSERT INTO career_levels_benefits VALUES (5, 24, 1);
INSERT INTO career_levels_benefits VALUES (5, 12, 500);
INSERT INTO career_levels_benefits VALUES (5, 13, 500);
INSERT INTO career_levels_benefits VALUES (5, 14, 1);
INSERT INTO career_levels_benefits VALUES (5, 15, 1);
INSERT INTO career_levels_benefits VALUES (5, 16, 5);
INSERT INTO career_levels_benefits VALUES (5, 21, 3);
INSERT INTO career_levels_benefits VALUES (5, 22, 8000);
INSERT INTO career_levels_benefits VALUES (2, 5, 1);


--
-- TOC entry 2281 (class 0 OID 24854)
-- Dependencies: 190
-- Data for Name: career_levels_todos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO career_levels_todos VALUES (2, 2, 5, 1);
INSERT INTO career_levels_todos VALUES (2, 3, 5, 1);
INSERT INTO career_levels_todos VALUES (2, 4, 5, 1);
INSERT INTO career_levels_todos VALUES (2, 5, 10, 1);
INSERT INTO career_levels_todos VALUES (2, 6, 10, 1);
INSERT INTO career_levels_todos VALUES (2, 7, 1, 0);
INSERT INTO career_levels_todos VALUES (2, 8, 10, 0);
INSERT INTO career_levels_todos VALUES (3, 2, 5, 1);
INSERT INTO career_levels_todos VALUES (3, 9, 10, 1);
INSERT INTO career_levels_todos VALUES (3, 8, 10, 1);
INSERT INTO career_levels_todos VALUES (3, 10, 20, 1);
INSERT INTO career_levels_todos VALUES (3, 7, 1, 0);
INSERT INTO career_levels_todos VALUES (3, 11, 10, 0);
INSERT INTO career_levels_todos VALUES (3, 12, 10, 0);
INSERT INTO career_levels_todos VALUES (3, 13, 25, 0);
INSERT INTO career_levels_todos VALUES (3, 15, 15, 0);
INSERT INTO career_levels_todos VALUES (4, 2, 5, 1);
INSERT INTO career_levels_todos VALUES (4, 9, 10, 1);
INSERT INTO career_levels_todos VALUES (4, 8, 10, 1);
INSERT INTO career_levels_todos VALUES (4, 10, 30, 1);
INSERT INTO career_levels_todos VALUES (4, 14, 5, 1);
INSERT INTO career_levels_todos VALUES (4, 7, 1, 0);
INSERT INTO career_levels_todos VALUES (4, 11, 10, 0);
INSERT INTO career_levels_todos VALUES (4, 12, 10, 0);
INSERT INTO career_levels_todos VALUES (4, 13, 25, 0);
INSERT INTO career_levels_todos VALUES (4, 15, 15, 0);
INSERT INTO career_levels_todos VALUES (5, 2, 5, 1);
INSERT INTO career_levels_todos VALUES (5, 9, 10, 1);
INSERT INTO career_levels_todos VALUES (5, 8, 10, 1);
INSERT INTO career_levels_todos VALUES (5, 10, 30, 1);
INSERT INTO career_levels_todos VALUES (5, 14, 5, 1);
INSERT INTO career_levels_todos VALUES (5, 7, 1, 0);
INSERT INTO career_levels_todos VALUES (5, 11, 10, 0);
INSERT INTO career_levels_todos VALUES (5, 12, 10, 0);
INSERT INTO career_levels_todos VALUES (5, 13, 25, 0);
INSERT INTO career_levels_todos VALUES (5, 15, 15, 0);
INSERT INTO career_levels_todos VALUES (4, 16, 10, 0);
INSERT INTO career_levels_todos VALUES (5, 16, 10, 0);
INSERT INTO career_levels_todos VALUES (1, 1, 10, 1);


--
-- TOC entry 2296 (class 0 OID 25168)
-- Dependencies: 205
-- Data for Name: event_attend; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO event_attend VALUES (20, 162, 3);
INSERT INTO event_attend VALUES (21, 162, 2);
INSERT INTO event_attend VALUES (22, 162, 4);
INSERT INTO event_attend VALUES (23, 162, 8);
INSERT INTO event_attend VALUES (24, 164, 2);
INSERT INTO event_attend VALUES (25, 164, 1);
INSERT INTO event_attend VALUES (26, 167, 160);
INSERT INTO event_attend VALUES (27, 167, 2);
INSERT INTO event_attend VALUES (28, 168, 160);
INSERT INTO event_attend VALUES (29, 168, 3);
INSERT INTO event_attend VALUES (30, 162, 170);
INSERT INTO event_attend VALUES (31, 168, 2);
INSERT INTO event_attend VALUES (32, 177, 160);
INSERT INTO event_attend VALUES (33, 179, 1);
INSERT INTO event_attend VALUES (34, 164, 3);
INSERT INTO event_attend VALUES (35, 177, 3);
INSERT INTO event_attend VALUES (36, 179, 160);
INSERT INTO event_attend VALUES (37, 163, 3);
INSERT INTO event_attend VALUES (38, 167, 3);
INSERT INTO event_attend VALUES (39, 179, 3);


--
-- TOC entry 2313 (class 0 OID 0)
-- Dependencies: 204
-- Name: event_attend_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('event_attend_id_seq', 39, true);


--
-- TOC entry 2314 (class 0 OID 0)
-- Dependencies: 202
-- Name: event_commentd_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('event_commentd_id_seq', 6, true);


--
-- TOC entry 2294 (class 0 OID 25154)
-- Dependencies: 203
-- Data for Name: event_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO event_comments VALUES (1, 'sdfsdf', 164, 3, '2017-07-10 10:52:23.412817');
INSERT INTO event_comments VALUES (2, 'cgfxfbxdc', 177, 3, '2017-07-10 10:52:55.241208');
INSERT INTO event_comments VALUES (3, 'hopa hopa', 179, 160, '2017-07-10 11:04:26.699517');
INSERT INTO event_comments VALUES (4, 'dfgdfg', 163, 3, '2017-07-10 11:11:40.841089');
INSERT INTO event_comments VALUES (5, 'dfgdgdfgdfg', 167, 3, '2017-07-10 11:17:22.9587');
INSERT INTO event_comments VALUES (6, 'dfgdfg', 179, 3, '2017-07-10 11:17:31.091059');


--
-- TOC entry 2298 (class 0 OID 25202)
-- Dependencies: 207
-- Data for Name: event_seen; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO event_seen VALUES (14, 162, 3);
INSERT INTO event_seen VALUES (15, 162, 2);
INSERT INTO event_seen VALUES (16, 162, 160);
INSERT INTO event_seen VALUES (17, 162, 4);
INSERT INTO event_seen VALUES (18, 162, 8);
INSERT INTO event_seen VALUES (19, 164, 2);
INSERT INTO event_seen VALUES (20, 164, 160);
INSERT INTO event_seen VALUES (21, 162, 1);
INSERT INTO event_seen VALUES (22, 164, 1);
INSERT INTO event_seen VALUES (23, 167, 160);
INSERT INTO event_seen VALUES (24, 167, 2);
INSERT INTO event_seen VALUES (25, 168, 160);
INSERT INTO event_seen VALUES (26, 168, 3);
INSERT INTO event_seen VALUES (27, 162, 170);
INSERT INTO event_seen VALUES (28, 164, 170);
INSERT INTO event_seen VALUES (29, 167, 170);
INSERT INTO event_seen VALUES (30, 168, 170);
INSERT INTO event_seen VALUES (31, 168, 2);
INSERT INTO event_seen VALUES (32, 177, 160);
INSERT INTO event_seen VALUES (33, 179, 1);
INSERT INTO event_seen VALUES (34, 164, 3);
INSERT INTO event_seen VALUES (35, 177, 3);
INSERT INTO event_seen VALUES (36, 179, 160);
INSERT INTO event_seen VALUES (37, 163, 3);
INSERT INTO event_seen VALUES (38, 167, 3);
INSERT INTO event_seen VALUES (39, 179, 3);


--
-- TOC entry 2315 (class 0 OID 0)
-- Dependencies: 206
-- Name: event_seen_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('event_seen_id_seq', 39, true);


--
-- TOC entry 2292 (class 0 OID 25137)
-- Dependencies: 201
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO events VALUES (162, 'Table tennis', 'Got to be cool!', 1, '2017-01-14 15:00:00', 'Stadion', 3, 3);
INSERT INTO events VALUES (163, 'Exp.', 'isdDescription', 1, '2017-01-06 22:59:00', 'Cuba', 1, 1);
INSERT INTO events VALUES (164, 'Handball', 'Take a ball', 1, '2017-01-13 14:00:00', 'Stadion', 2, 2);
INSERT INTO events VALUES (167, 'tfhgfghj', 'fghjfghjfhgj', 1, '2017-01-06 21:00:00', 'fghjfgh', 160, 160);
INSERT INTO events VALUES (168, 'sdfgsdfg', 'dsfgsdfgsdfg', 1, '2017-01-09 09:00:00', 'fdgsdfg', 160, 160);
INSERT INTO events VALUES (177, 'Ping Pong', 'adfsdfg', 1, '2017-01-19 00:00:00', 'cgdfg', 160, 160);
INSERT INTO events VALUES (179, 'Special', 'special', 1, '2017-01-09 22:59:00', 'Niger', 1, 1);


--
-- TOC entry 2316 (class 0 OID 0)
-- Dependencies: 200
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('events_id_seq', 5, true);


--
-- TOC entry 2282 (class 0 OID 24861)
-- Dependencies: 191
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO groups VALUES (1, 'USER');
INSERT INTO groups VALUES (2, 'ADMIN');


--
-- TOC entry 2317 (class 0 OID 0)
-- Dependencies: 185
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hibernate_sequence', 179, true);


--
-- TOC entry 2285 (class 0 OID 24954)
-- Dependencies: 194
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO notification VALUES (28, 3, 3, NULL, 162);
INSERT INTO notification VALUES (29, 2, 3, NULL, 164);
INSERT INTO notification VALUES (30, 160, 3, NULL, 167);
INSERT INTO notification VALUES (31, 160, 3, NULL, 168);
INSERT INTO notification VALUES (32, 160, 3, NULL, 177);
INSERT INTO notification VALUES (33, 1, 3, NULL, 179);


--
-- TOC entry 2318 (class 0 OID 0)
-- Dependencies: 199
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('notification_id_seq', 33, true);


--
-- TOC entry 2319 (class 0 OID 0)
-- Dependencies: 195
-- Name: promote_notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('promote_notification_id_seq', 10, true);


--
-- TOC entry 2287 (class 0 OID 25003)
-- Dependencies: 196
-- Data for Name: responsibility_cards; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO responsibility_cards VALUES (42, '/images/m3.png', 'New features!', 'Amazing speed!', 'Integrity', 170);
INSERT INTO responsibility_cards VALUES (78, '/images/m17.png', 'ertghf', 'etygg', 'ertfgh', 3);
INSERT INTO responsibility_cards VALUES (84, '/images/m11.png', 'asdw', 'asdw', 'asdw', 270);
INSERT INTO responsibility_cards VALUES (43, '/images/m2.png', 'Ivanov''s Description', 'Ivanov''s Expected Result', 'Funn', 4);
INSERT INTO responsibility_cards VALUES (97, '/images/mdfgh7.png', 'fgbfgbxd', 'szdfvbzsdfv', 'fgbsrfgb', 1);
INSERT INTO responsibility_cards VALUES (98, '/images/mdfgh7.png', 'dfgbsxdfv ', 'rdfvzdsv', 'fsgd', 1);
INSERT INTO responsibility_cards VALUES (99, '/images/m13456.png', 'ncghncghnb', 'cghfnbcghnb ', 'cgvbncgvb', 1);
INSERT INTO responsibility_cards VALUES (101, '/images/wikipedia_64x64[1].png', 'cvbncvbn', 'cvbncvbn', 'vb cbvn', 1);
INSERT INTO responsibility_cards VALUES (104, '/images/dfsg.png', 'gsdfgsdfg', 'sdfgsdfg', 'resfsdf', 1);
INSERT INTO responsibility_cards VALUES (105, '/images/mgdfh8.png', 'sdfgsdfg', 'dsfgsdfgsdfg', 'sdfgsdfg', 1);
INSERT INTO responsibility_cards VALUES (106, '/images/uiouio.png', 'dfghdfghsssw', 'dfghdfghww', 'dfghdfghjhnjm,m', 1);
INSERT INTO responsibility_cards VALUES (107, '/images/345434.png', 'dfghdfgh', 'dfghdfgh', 'dhgdfg', 1);
INSERT INTO responsibility_cards VALUES (108, '/images/789.png', 'cxvxcvbxcvb', 'xcvbxcvb', 'fgdfgb', 1);
INSERT INTO responsibility_cards VALUES (109, '/images/345sdsd434.png', 'hdfghdfgh', 'dfghdfgh', 'dghfghdfg', 1);
INSERT INTO responsibility_cards VALUES (110, '/images/sdsdfesa.png', 'fghjfghjfg', 'hjfghjf', 'fghjfghj', 1);
INSERT INTO responsibility_cards VALUES (112, '/images/78989759.png', 'gfdgdd', 'xfgbfshf', 'dfgdgsdsdd', 1);
INSERT INTO responsibility_cards VALUES (113, '/images/rtyrty.png', 'xgfxvc', 'gfvxc', 'ghdzdf', 1);
INSERT INTO responsibility_cards VALUES (114, '/images/fewrfgw45g.png', 'dfgbdfgbd', 'dfgbdfgbdfg', 'fgfgbdgfh', 1);
INSERT INTO responsibility_cards VALUES (92, '/images/m14.png', 'jokes aside', 'try not to laught', 'have fun', 9);
INSERT INTO responsibility_cards VALUES (111, '/images/dd.png', 'dsfgsdfg', 'dfgsdfg', 'dfghgdsfh', 160);
INSERT INTO responsibility_cards VALUES (102, '/images/mdfgh7.png', 'dsfgsdfgsdf', 'gdsfgsdfg', 'sdfvsdfg', 160);
INSERT INTO responsibility_cards VALUES (94, '/images/m13456.png', 'fgdxbgbxfgb', 'fdgbxfgb', 'fgdgxdfb', 160);
INSERT INTO responsibility_cards VALUES (103, '/images/wefwe.png', 'dsfgsdfgsdfg', 'sdfgsdfgsdfg', 'dfgsfgsdfg', 160);
INSERT INTO responsibility_cards VALUES (95, '/images/Screenshot_2.png', 'nxfngx', 'gnxcfgncxhfn', 'xfcgvbx', 160);
INSERT INTO responsibility_cards VALUES (82, '/images/m6.png', 'Be cool everyday.', 'Be Respected.', 'Respect', 4);
INSERT INTO responsibility_cards VALUES (96, '/images/mgdfh8.png', 'cghnbcgfb', 'ghnxfgb', 'ghcncgvb', 2);


--
-- TOC entry 2320 (class 0 OID 0)
-- Dependencies: 197
-- Name: responsibility_cards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('responsibility_cards_id_seq', 1, false);


--
-- TOC entry 2277 (class 0 OID 24747)
-- Dependencies: 186
-- Data for Name: todo_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO todo_types VALUES (1, 'Prove you are proactive');
INSERT INTO todo_types VALUES (2, 'Daily time tracking report');
INSERT INTO todo_types VALUES (3, 'Read "Warehouse & Distribution Science"');
INSERT INTO todo_types VALUES (4, 'Online agile course');
INSERT INTO todo_types VALUES (5, 'Assessment result above 3');
INSERT INTO todo_types VALUES (6, '3 Months in the company');
INSERT INTO todo_types VALUES (7, 'Ideas for the company');
INSERT INTO todo_types VALUES (8, 'Article or Presentation');
INSERT INTO todo_types VALUES (9, 'Assessment reuslt above 3.5');
INSERT INTO todo_types VALUES (10, 'English Advanced');
INSERT INTO todo_types VALUES (11, 'Book or Course');
INSERT INTO todo_types VALUES (12, 'Conference or Training');
INSERT INTO todo_types VALUES (13, 'Teamlead');
INSERT INTO todo_types VALUES (14, 'Buddy');
INSERT INTO todo_types VALUES (15, 'Responsibility Card');
INSERT INTO todo_types VALUES (16, 'Presentation or Article');


--
-- TOC entry 2280 (class 0 OID 24847)
-- Dependencies: 189
-- Data for Name: todos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO todos VALUES (1, 1, 'I did it', 1, '2017-04-09 19:31:27', 'It was interesting!');
INSERT INTO todos VALUES (2, 1, 'I did it!', 1, '2017-04-09 20:27:49', 'It was very interesting.');
INSERT INTO todos VALUES (2, 2, 'Tracking report', 4, '2017-04-09 19:37:07', 'Interesting report.');
INSERT INTO todos VALUES (2, 2, 'trial period', 2, '2017-04-09 18:29:03', 'Doing stuff.');
INSERT INTO todos VALUES (4, 2, 'trial period', 2, '2017-04-09 18:29:03', 'Doing stuff.');
INSERT INTO todos VALUES (4, 2, 'Good book', 3, '2017-04-09 18:29:03', 'Learned a lot.');
INSERT INTO todos VALUES (2, 2, '3 Month', 6, '2017-01-30 00:06:00', 'It was awesome');
INSERT INTO todos VALUES (2, 2, 'dfg', 5, '2017-01-30 00:06:00', 'dfg');
INSERT INTO todos VALUES (2, 2, 'sdrfg', 5, '2017-01-30 00:06:00', 'dfg');
INSERT INTO todos VALUES (2, 5, 'dsf', 8, '2017-01-03 00:07:00', 'sdf');
INSERT INTO todos VALUES (2, 5, 'asdzx', 2, '2017-01-08 00:07:00', 'asdqe');
INSERT INTO todos VALUES (2, 5, '123123', 14, '2017-01-27 00:07:00', '123123');


--
-- TOC entry 2283 (class 0 OID 24868)
-- Dependencies: 192
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users VALUES (1, 'Goodman', 'John', 'admin', '$2a$10$y4tU2GY6BGz6/B.qMbs.6eBetMOHW2SkNhnOOq/y/Id5NaTYW37DO', 'goodman@gmail.com', 1, 2);
INSERT INTO users VALUES (4, 'Public', 'Mary', 'public123', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'mary@mail.com', 2, 1);
INSERT INTO users VALUES (5, 'Queue', 'Susan', 'queue1111', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'susan@mail.com', 3, 1);
INSERT INTO users VALUES (6, 'Jackson', 'Michael', 'jackson1111', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'michael@mail.com', 4, 1);
INSERT INTO users VALUES (1700, 'afsafff', 'dfsadfadfsaf', 'adfadfaf', '$2a$10$MZV1aFI/9lC.P2TLX3DNMe9RBMbFTvJvhe4CUP9LrC07U9C8zgzju', 'safdafs@adfaf.com', 1, 1);
INSERT INTO users VALUES (2, 'Ivanov', 'Alexandr', 'user', '$2a$10$xZV57Td6nvKOnTsW986XO.j2dr10gow506U1.fC1u0aJMB6XtJG6C', 'ivanov@gmail.com', 5, 1);
INSERT INTO users VALUES (1750, 'hgf', 'lkhgf', 'faashh', '$2a$10$4V6QY8gsitkalANzpCI5oe3goNjzTwiw3n8zyUOINjgV5J/PAZKKK', 'asfa@afa.cpm', 1, 1);
INSERT INTO users VALUES (1751, 'lkjhg', 'lkjhg', 'jkhgfde', '$2a$10$XIWCuLi/iXqSXai3ipeqLOG67CTn5AK2I2OXdvUlSMxlqLkDprx0W', 'ghjh@klkjhg.com', 1, 1);
INSERT INTO users VALUES (1760, 'sdcac', 'jhgcasc', 'afasfa', '$2a$10$mHiS92VGsBJSl3hRUJH9ve5zxP8FEP4MVi0B21g/6SVXyyynIycG6', 'dasfw@afsdf.com', 1, 1);
INSERT INTO users VALUES (1780, 'fadsf', 'jhgf', 'sadfsadf', '$2a$10$YeaZQBFkOZQ1CZU2mQuoRuKVQ4AGnvq/HjYjSGXYR0zFjV2Ciyv5e', 'asfdsa@fff.com', 1, 1);
INSERT INTO users VALUES (270, 'oooo', 'oooo', 'oooo', '$2a$10$xZV57Td6nvKOnTsW986XO.j2dr10gow506U1.fC1u0aJMB6XtJG6C', 'oooo@gmail.com', 4, 1);
INSERT INTO users VALUES (170, 'hello', 'hello', 'hello', '$2a$10$erXhI2iEmM2YvYKUW8NZHefsYWiBslcaNzO1NoOpGBLtRBE.YJUGe', 'hello@yahoo.com', 3, 1);
INSERT INTO users VALUES (8, 'Smith', 'James', 'smith123', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'james@mail.com', 4, 1);
INSERT INTO users VALUES (3, 'Doe', 'John', 'john123', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'john@mail.com', 1, 1);
INSERT INTO users VALUES (10, 'Black', 'Arnold', 'arnold1111', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'black@mail.com', 1, 1);
INSERT INTO users VALUES (160, 'jora', 'jora', 'jora', '$2a$10$xZV57Td6nvKOnTsW986XO.j2dr10gow506U1.fC1u0aJMB6XtJG6C', 'jora@gmail.com', 4, 1);
INSERT INTO users VALUES (150, 'White', 'Bernie', 'bernie', '$2a$10$E5n.QcmEaX18xQGutDOvBeGIFT3XPfTy.M/On8nGkmL7RNsj9sjr6', 'bernie@mail.com', 5, 1);
INSERT INTO users VALUES (880, 'mike', 'ionel', 'ionel', '$2a$10$Iel8XeVLeJypaQvin0e8COZYb.LW/RJb8WaERFFrBw1fnJ30hLPb2', 'ionel@gmail.com', 3, 1);
INSERT INTO users VALUES (9, 'Sweater', 'Peter', 'sweater123', '$2a$06$3jYRJrg0ghaaypjZ/.g4SethoeA51ph3UD4kZi9oPkeMTpjKU5uo6', 'sweater@mail.com', 3, 1);
INSERT INTO users VALUES (1690, 'player one', 'ready', 'player', '$2a$10$.fQOYmYT6E3ayYlKNG12hOuUq6BoAn7prUeuusGBaJtJKlj3d2QTS', 'one@hello.com', 1, 1);


--
-- TOC entry 2100 (class 2606 OID 24846)
-- Name: benefit_types benefit_types_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY benefit_types
    ADD CONSTRAINT benefit_types_name_key UNIQUE (name);


--
-- TOC entry 2102 (class 2606 OID 24844)
-- Name: benefit_types benefit_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY benefit_types
    ADD CONSTRAINT benefit_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2125 (class 2606 OID 25095)
-- Name: assignments card_assign_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT card_assign_history_pkey PRIMARY KEY (id);


--
-- TOC entry 2098 (class 2606 OID 24839)
-- Name: career_levels_benefits career_levels_benefits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels_benefits
    ADD CONSTRAINT career_levels_benefits_pkey PRIMARY KEY (career_level_id, benefit_type_id);


--
-- TOC entry 2116 (class 2606 OID 24881)
-- Name: career_levels career_levels_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels
    ADD CONSTRAINT career_levels_name_key UNIQUE (name);


--
-- TOC entry 2118 (class 2606 OID 24879)
-- Name: career_levels career_levels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels
    ADD CONSTRAINT career_levels_pkey PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 24860)
-- Name: career_levels_todos career_levels_todos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels_todos
    ADD CONSTRAINT career_levels_todos_pkey PRIMARY KEY (career_level_id, todo_type_id);


--
-- TOC entry 2131 (class 2606 OID 25173)
-- Name: event_attend event_attend_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_attend
    ADD CONSTRAINT event_attend_pkey PRIMARY KEY (id);


--
-- TOC entry 2129 (class 2606 OID 25160)
-- Name: event_comments event_commentd_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_comments
    ADD CONSTRAINT event_commentd_pkey PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 25207)
-- Name: event_seen event_seen_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_seen
    ADD CONSTRAINT event_seen_pkey PRIMARY KEY (id);


--
-- TOC entry 2127 (class 2606 OID 25151)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 2108 (class 2606 OID 24865)
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2110 (class 2606 OID 24867)
-- Name: groups groups_role_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_role_key UNIQUE (role);


--
-- TOC entry 2121 (class 2606 OID 24958)
-- Name: notification promote_notification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT promote_notification_pkey PRIMARY KEY (id);


--
-- TOC entry 2123 (class 2606 OID 25011)
-- Name: responsibility_cards responsibility_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY responsibility_cards
    ADD CONSTRAINT responsibility_cards_pkey PRIMARY KEY (id);


--
-- TOC entry 2094 (class 2606 OID 24753)
-- Name: todo_types todo_types_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY todo_types
    ADD CONSTRAINT todo_types_name_key UNIQUE (name);


--
-- TOC entry 2096 (class 2606 OID 24751)
-- Name: todo_types todo_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY todo_types
    ADD CONSTRAINT todo_types_pkey PRIMARY KEY (id);


--
-- TOC entry 2104 (class 2606 OID 24853)
-- Name: todos todos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (user_id, career_level_id, todo_id);


--
-- TOC entry 2112 (class 2606 OID 24872)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2114 (class 2606 OID 24874)
-- Name: users users_username_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_email_key UNIQUE (username, email);


--
-- TOC entry 2119 (class 1259 OID 24964)
-- Name: promote_notification_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX promote_notification_id_uindex ON notification USING btree (id);


--
-- TOC entry 2148 (class 2606 OID 25101)
-- Name: assignments card_assign_history_responsibility_cards_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT card_assign_history_responsibility_cards_id_fk FOREIGN KEY (card_id) REFERENCES responsibility_cards(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2147 (class 2606 OID 25096)
-- Name: assignments card_assign_history_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT card_assign_history_users_id_fk FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2139 (class 2606 OID 24897)
-- Name: career_levels_todos fk1mq1cnfg0u6pwvtfbo54okhs7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels_todos
    ADD CONSTRAINT fk1mq1cnfg0u6pwvtfbo54okhs7 FOREIGN KEY (career_level_id) REFERENCES career_levels(id);


--
-- TOC entry 2149 (class 2606 OID 25106)
-- Name: assignments fk3f00pti9idrc21cnauumcic5w; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT fk3f00pti9idrc21cnauumcic5w FOREIGN KEY (card_id) REFERENCES responsibility_cards(id);


--
-- TOC entry 2144 (class 2606 OID 25189)
-- Name: notification fk62mnpgloeus6qb2j7cby0gid5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT fk62mnpgloeus6qb2j7cby0gid5 FOREIGN KEY (event_id) REFERENCES events(id);


--
-- TOC entry 2142 (class 2606 OID 25122)
-- Name: notification fk64qor2dn1apbjf7rgnkw9qm89; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT fk64qor2dn1apbjf7rgnkw9qm89 FOREIGN KEY (req_card_id) REFERENCES responsibility_cards(id);


--
-- TOC entry 2140 (class 2606 OID 24912)
-- Name: users fk6b2cs8xemrkie24tsbkql5s44; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk6b2cs8xemrkie24tsbkql5s44 FOREIGN KEY (career_level_id) REFERENCES career_levels(id);


--
-- TOC entry 2152 (class 2606 OID 25184)
-- Name: events fk7gaujeodsxymr1fs9h0uy4wtq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events
    ADD CONSTRAINT fk7gaujeodsxymr1fs9h0uy4wtq FOREIGN KEY (author_id) REFERENCES users(id);


--
-- TOC entry 2150 (class 2606 OID 25111)
-- Name: assignments fk8iy60sfbpptmtm10f4al96qvq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY assignments
    ADD CONSTRAINT fk8iy60sfbpptmtm10f4al96qvq FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2136 (class 2606 OID 24902)
-- Name: todos fk9605g76a1dggbvs18f2r80gvu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY todos
    ADD CONSTRAINT fk9605g76a1dggbvs18f2r80gvu FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2134 (class 2606 OID 24882)
-- Name: career_levels_benefits fk97w1nju0fpm8ngr4a7km9l1ha; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels_benefits
    ADD CONSTRAINT fk97w1nju0fpm8ngr4a7km9l1ha FOREIGN KEY (benefit_type_id) REFERENCES benefit_types(id);


--
-- TOC entry 2137 (class 2606 OID 24907)
-- Name: todos fka43ince4ply4ch04kv8jivpvy; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY todos
    ADD CONSTRAINT fka43ince4ply4ch04kv8jivpvy FOREIGN KEY (career_level_id) REFERENCES career_levels(id);


--
-- TOC entry 2158 (class 2606 OID 25213)
-- Name: event_seen fkav9u1fre5r0kh7s8c66oga4pg; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_seen
    ADD CONSTRAINT fkav9u1fre5r0kh7s8c66oga4pg FOREIGN KEY (event_id) REFERENCES events(id);


--
-- TOC entry 2138 (class 2606 OID 24892)
-- Name: career_levels_todos fkc6isbapaf34bovy5mprhp9rrh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels_todos
    ADD CONSTRAINT fkc6isbapaf34bovy5mprhp9rrh FOREIGN KEY (todo_type_id) REFERENCES todo_types(id);


--
-- TOC entry 2141 (class 2606 OID 24917)
-- Name: users fkemfuglprp85bh5xwhfm898ysc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fkemfuglprp85bh5xwhfm898ysc FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- TOC entry 2146 (class 2606 OID 25085)
-- Name: responsibility_cards fklvrmahoxumlls3qllxctnlsl7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY responsibility_cards
    ADD CONSTRAINT fklvrmahoxumlls3qllxctnlsl7 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2154 (class 2606 OID 25238)
-- Name: event_comments fkmc6b51f0imw3rw442hqwk3wwh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_comments
    ADD CONSTRAINT fkmc6b51f0imw3rw442hqwk3wwh FOREIGN KEY (event_id) REFERENCES events(id);


--
-- TOC entry 2135 (class 2606 OID 24887)
-- Name: career_levels_benefits fkn5g97vm2pw15r1hivlxh3a5ge; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY career_levels_benefits
    ADD CONSTRAINT fkn5g97vm2pw15r1hivlxh3a5ge FOREIGN KEY (career_level_id) REFERENCES career_levels(id);


--
-- TOC entry 2156 (class 2606 OID 25179)
-- Name: event_attend fkn66c1uuss9a187n0de5j02rsp; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_attend
    ADD CONSTRAINT fkn66c1uuss9a187n0de5j02rsp FOREIGN KEY (event_id) REFERENCES events(id);


--
-- TOC entry 2143 (class 2606 OID 25127)
-- Name: notification fknk4ftb5am9ubmkv1661h15ds9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY notification
    ADD CONSTRAINT fknk4ftb5am9ubmkv1661h15ds9 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2153 (class 2606 OID 25233)
-- Name: event_comments fkpk2e2p2xoj94r43co8b06xato; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_comments
    ADD CONSTRAINT fkpk2e2p2xoj94r43co8b06xato FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2155 (class 2606 OID 25174)
-- Name: event_attend fkpy4m3polhyxk1demnwl167fgk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_attend
    ADD CONSTRAINT fkpy4m3polhyxk1demnwl167fgk FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2151 (class 2606 OID 25161)
-- Name: events fkqb2sh2nyr3kdqgutaqll5cfpr; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY events
    ADD CONSTRAINT fkqb2sh2nyr3kdqgutaqll5cfpr FOREIGN KEY (author_id) REFERENCES users(id);


--
-- TOC entry 2157 (class 2606 OID 25208)
-- Name: event_seen fkrdd4bc63emk4kt6p07id64rgw; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY event_seen
    ADD CONSTRAINT fkrdd4bc63emk4kt6p07id64rgw FOREIGN KEY (user_id) REFERENCES users(id);


--
-- TOC entry 2145 (class 2606 OID 25080)
-- Name: responsibility_cards responsibility_cards_users_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY responsibility_cards
    ADD CONSTRAINT responsibility_cards_users_id_fk FOREIGN KEY (user_id) REFERENCES users(id);


-- Completed on 2017-07-10 11:28:11

--
-- PostgreSQL database dump complete
--

