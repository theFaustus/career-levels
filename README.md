# Career Levels
This is an attempt to develop an already existing project called Career Levels which is a program for tracking the employee progress at his career journey. 
Every user has to complete tasks to go to the next level. The program shows all tasks and benefits for each user. User with admin role can add, edit or remove tasks, benefits and user accounts. Regular user can edit only his finished tasks.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
This project was developed using: [Intellij IDEA](https://www.jetbrains.com/idea/) and [PostgreSQL 9.6 ORDBMS](https://www.postgresql.org/).

### Installing
1. Run [Intellij IDEA 2016 (Any other version)](https://www.jetbrains.com/idea/).
2. Open it and:
   1. File -> Project from existing sources.
   2. Browse to the location of extracted project.
   3. Choose Maven as an building tool.
   4. Click Finish.
3. Install [PostgreSQL 9.6 ORDBMS](https://www.postgresql.org/)
4. From PgAdmin start PostgreSQL 9.6 Server and create a database (righ click on Databases -> Create).
5. Before running application configure your application.properties file to set up your database:
   check your database name (PostgreSQL), replace the user name and password of your database.
   For example:
      * `# Connection url for the database "career_levels" on port 5433`
      * `spring.datasource.url = jdbc:postgresql://172.17.41.137:5433/career_levels`
      * `spring.datasource.driver-class-name=org.postgresql.Driver`

      * `# Username and password`
      * `spring.datasource.username = postgres`
      * `spring.datasource.password = 123456`
6. Once you`re all set up, hit SHIFT + F10 or Run -> Run 'CareerLevelsApplication'
7. Open your favorite web browser and type in address bar: http://localhost:8080/
8. To login there are two accounts for your convenience:
  #### Admin account:
  * Username: admin
  * Password: admin
  
  #### Regular user account:
  * Username: user
  * Password: user

## Deployment
If everything went well till now :) and you want to deploy this application you`ll need to:
1. Download [Tomcat 8.5.11 (The one that`s embedded into the Spring Boot)](https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.11/bin/).
2. Install it. And take care about :
	* The location of your server should be somewhere where app have the access to write and read. Something like : C:\Users\Faust\ApacheFoundations\Tomcat .
	* If you chose another location, make sure to untick the Read-Only property of your desired folder.
	* In rest clicking NEXT will do the game.
3. Now you`ll need to configure your server.xml file.
	Add a <Context> element within the <Host> like below which sets your CareerLevels as the default web app. Note the empty path="" which makes it the default.
	`<Context docBase="CareerLevels" path="" />` 
4. After a successful installation, go to Intellij
	1. Open the terminal.
	2. Type in `mvn clean install`
	3. Check the location shown by maven where the .war file appeared.
	4. Copy *.war file to `${Tomcat 8 location}/webapps`.
5. To start your application go to `${Tomcat 8 location}/bin`. And :
	* Either use the startup.bat file and start the server in cmd.
	* Or go to Windows -> Search an type in `Configure Tomcat`, launch it and click Start.
6. Once everythingis ready open your favorite web browser and type in address bar: http://your-ip-address:8080/

## Original Authors
* Alexandr Jlobici
* Alexandru Rotari

## Current Authors
* Ion Pascari
* Radu Madiudin
* Artiom Nichifor
* Alexandru Malina

## License
This project is licensed under the MIT License - see the LICENSE.txt file for details.
