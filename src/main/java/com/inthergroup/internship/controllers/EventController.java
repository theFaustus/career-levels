package com.inthergroup.internship.controllers;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.EventAttend;
import com.inthergroup.internship.models.User;
import com.inthergroup.internship.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * Created by Faust on 7/6/2017.
 */
@Controller
public class EventController {
    @Autowired
    private UserService userService;
    @Autowired
    private ResponsibilityCardsService responsibilityCardsService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private AssignmentService assignmentService;
    @Autowired
    private EventService eventService;


    @RequestMapping("/events/all-events")
    public String cards(Model model) {
        //Get the current user
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }
        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }
        model.addAttribute("allUsers", userService.findAll());
        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());
        model.addAttribute("cards", responsibilityCardsService.findAllResponsibilityCards());
        List<Event> allAvailableEvents = eventService.findAllAvailableEvents();
        Collections.sort(allAvailableEvents, (o1, o2) -> o1.getDate().compareTo(o2.getDate()));
        model.addAttribute("availableEvents", allAvailableEvents);
        List<Object[]> allAttenders = eventService.findAllAttenders();
        model.addAttribute("attenders", allAttenders);
        model.addAttribute("eventAttends", eventService.findAllEventAttends());
        System.out.println(eventService.findAllEventAttends());
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext(); ) {
                    Object[] o = it.next();
                    if (seenEvents.contains(o[2])) {
                        it.remove();
                    }
                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }
        List<EventAttend> allEventAttends = eventService.findAllEventAttends();
        Map<Long, Boolean> mapOfEventsThatCurrentUserVisited = new HashMap<>();
        User user = userService.findByUsername(currentUserName);

        for (Event e : allAvailableEvents) {
            for (EventAttend et : allEventAttends) {
                if (Objects.equals(e.getId(), et.getEvent().getId()) && (et.getAttender().getId() == user.getId())) {
                    mapOfEventsThatCurrentUserVisited.put(e.getId(), true);
                    break;
                } else
                    mapOfEventsThatCurrentUserVisited.put(e.getId(), false);
            }
        }

        Map<Long, Long> numberOfAttendersPerEvent = new HashMap<>();
        Long sumOfAttendersPerEvent = 0L;
        for(Event e : allAvailableEvents) {
            sumOfAttendersPerEvent = 0L;
            for(Object[] o : allAttenders){
                if(Objects.equals(o[0], e.getId()) && !(Objects.equals(((User) o[1]).getId(), e.getUserAuthor().getId()))) {
                    sumOfAttendersPerEvent++;
                }
            }
            numberOfAttendersPerEvent.put(e.getId(), sumOfAttendersPerEvent);
        }

        model.addAttribute("numberOfAttendersPerEvent", numberOfAttendersPerEvent);
        model.addAttribute("eventAttendMap", mapOfEventsThatCurrentUserVisited);

        return "/events/all-events";
    }

    @RequestMapping("/remove-event")
    @Transactional
    @Modifying
    public String deleteEvent(Long id) {
        try {
            System.out.println(id);

            eventService.deleteEventAttendByEventId(id);
            eventService.deleteEventCommentByEventId(id);
            eventService.deleteEventSeenByEventId(id);
            notificationService.deleteEventNotificationByEventId(id);
            eventService.deleteById(id);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error deleting the event card by id #" + id + ": " + ex.toString();
        }
        return "redirect:/events/all-events";
    }

    @RequestMapping("/remove-comment")
    @Transactional
    @Modifying
    public String deleteComment(Long id) {
        try {
            System.out.println(id);
            eventService.updateEventCommentByCommentId(id,"");
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error deleting the event card by id #" + id + ": " + ex.toString();
        }
        return "redirect:/events/all-events";
    }

    @RequestMapping("/update-comment")
    @Transactional
    @Modifying
    public String updateComment(@RequestParam("updateCommentId") Long id, @RequestParam("updateCommentText") String text) {
        try {
            System.out.println(id);
            eventService.updateEventCommentByCommentId(id, text);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error deleting the event card by id #" + id + ": " + ex.toString();
        }
        return "redirect:/events/all-events";
    }

    @RequestMapping("/leave-event")
    @Transactional
    @Modifying
    public String leaveEvent(Long leaveEventId, Long leaveUserId) {
        try {
            System.out.println(leaveEventId);
            System.out.println(leaveUserId);
            eventService.deleteEventAttendByEventIdAndByUserId(leaveEventId, leaveUserId);
            eventService.deleteEventCommentByEventIdAndUserId(leaveEventId, leaveUserId);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error leaving the event card by id #" + leaveEventId + ": " + ex.toString();
        }
        return "redirect:/events/all-events";
    }


    @RequestMapping(value = "/create-event")
    public String createResponsibilityCard(@RequestParam("title") String title,
                                           @RequestParam("description") String description,
                                           @RequestParam("eventDate") @DateTimeFormat(pattern = "yyyy/mm/dd HH:mm") Date date,
                                           @RequestParam("place") String place) {

        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
        }
        User user = userService.findByUsername(currentUserName);
        Event event = null;

        event = new Event(title, description, 1L, date, place, user, user.getId());
        event = eventService.save(event);
        Integer attendEvent = eventService.findEventAttendByUserIdAndEventId(user.getId(), event.getId());
        if (attendEvent == null)
            eventService.createEventAttend(event.getId(), user.getId());
        Integer seenEvent = eventService.findEventSeenByUserIdAndEventId(user.getId(), event.getId());
        if (seenEvent == null)
            eventService.createEventSeen(event.getId(), user.getId());
        notificationService.createEventNotificationFromParams(user.getId(), event.getId());


        return "redirect:/events/all-events";
    }

    @RequestMapping(value = "/deny-event")
    public String denyEvent(@RequestParam("denyEventId") Long eventId,
                            @RequestParam("denyUserId") Long userId
    ) {
        Integer seenEvent = eventService.findEventSeenByUserIdAndEventId(userId, eventId);
        if (seenEvent == null)
            eventService.createEventSeen(eventId, userId);
        return "redirect:/events/all-events";
    }

    @RequestMapping(value = "/join-event")
    public String joinEvent(@RequestParam("joinEventId") Long eventId,
                            @RequestParam("joinUserId") Long userId,
                            @RequestParam("joinCommentText") String text
    ) {
        Integer attendEvent = eventService.findEventAttendByUserIdAndEventId(userId, eventId);
        if (attendEvent == null)
            eventService.createEventAttend(eventId, userId);
        Integer seenEvent = eventService.findEventSeenByUserIdAndEventId(userId, eventId);
        if (seenEvent == null)
            eventService.createEventSeen(eventId, userId);
        Integer commentEvent = eventService.findEventCommentByUserIdAndEventId(userId, eventId);
        if (text.isEmpty())
            text = "Cool! I`m in!";
        if (commentEvent == null) {
            eventService.createEventComment(eventId, userId, text);
        } else {
            eventService.deleteEventCommentByEventIdAndUserId(eventId, userId);
            eventService.createEventComment(eventId, userId, text);
        }
        return "redirect:/events/all-events";
    }

    @RequestMapping("/edit-event")
    public String editEvent(long eventId,
                            String titleEdit,
                            String descriptionEdit,
                            @RequestParam("dateEdit") @DateTimeFormat(pattern = "yyyy/mm/dd HH:mm") Date dateEdit,
                            String placeEdit) {
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
        }
        User user = userService.findByUsername(currentUserName);
        Event event = null;
        System.out.println("Event ID: " + eventId);
        try {
            event = eventService.findById(eventId);
            event.setName(titleEdit);
            event.setDescription(descriptionEdit);
            event.setDate(dateEdit);
            event.setPlace(placeEdit);
            eventService.edit(event);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error creating the Event: " + ex.toString();
        }
        return "redirect:/events/all-events";
    }
}
