package com.inthergroup.internship.controllers;

import javax.validation.Valid;

import com.inthergroup.internship.models.*;
import com.inthergroup.internship.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.inthergroup.internship.forms.SignupForm;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


@Controller
public class UserController {

    // ------------------------
    // PRIVATE FIELDS
    // ------------------------

    @Autowired
    private UserService userService;

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private CareerLevelService careerLevelService;

    @Autowired
    private TodoService todoService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ResponsibilityCardsService responsibilityCardsService;

    @Autowired
    private EventService eventService;


    // ------------------------
    // PUBLIC METHODS
    // ------------------------

    @RequestMapping("/login")
    public String login() {
        return "/users/login";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        model.addAttribute("signupForm", new SignupForm());
        return "users/signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupUser(
            @Valid @ModelAttribute("signupForm") SignupForm signupForm,
            BindingResult bindingResult) {
        System.out.println(bindingResult.toString());
        if (!bindingResult.hasErrors()) { // validation errors
            if (signupForm.getPassword().equals(signupForm.getPasswordCheck())) { // check password match
                String pwd = signupForm.getPassword();
                BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
                String hashPwd = bc.encode(pwd);

                User newUser = new User();
                newUser.setFirstName(signupForm.getFirstName());
                newUser.setLastName(signupForm.getLastName());
                newUser.setEmail(signupForm.getEmail());
                newUser.setUsername(signupForm.getUsername());
                newUser.setPasswordHash(hashPwd);

                CareerLevel careerLevel = careerLevelService.findById((long) 1); // id=1 for career level "Internship"
                newUser.setCareerLevel(careerLevel);

                Group group = groupService.findByRole("USER");
                newUser.setGroup(group);

                if (userService.findByUsername(signupForm.getUsername()) == null) {
                    userService.create(newUser);
                } else {
                    bindingResult.rejectValue("username", "error.userexists", "Username already exists");
                    return "/users/signup";
                }
            } else {
                bindingResult.rejectValue("passwordCheck", "error.pwdmatch", "Passwords does not match");
                return "/users/signup";
            }
        } else {
            return "/users/signup";
        }
        return "redirect:/login";
    }

    /**
     * Prepares data for page with all users list.
     *
     * @param model
     * @return
     */
    @RequestMapping("/users/all-users")
    public String allUsers(Model model) {
        //Get the current user
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }
        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }
        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());
        model.addAttribute("users", userService.findAll());
        model.addAttribute("careerLevels", careerLevelService.findAll());
        model.addAttribute("groups", groupService.findAll());
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext(); ) {
                    Object[] o = it.next();
                    if (seenEvents.contains(o[2])) {
                        it.remove();
                    }

                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }
        return "/users/all-users";
    }

    /**
     * Prepares data for page with current progress for specific user.
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/users/progress-page/{id}")
    public String progressPage(@PathVariable("id") Long id, Model model) {
        Long careerLevelId = careerLevelService.findCareerLevelByUserId(id).getId();
        System.out.println(careerLevelId);
        //Get the current user
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }


        Map<String, Integer> mandatoryTasksDone = userService.findUsersMandatoryTaskDone();
        Integer userMandatoryTasksDone = mandatoryTasksDone.get(String.valueOf(id));
        Map<String, Integer> totalTasks = userService.findUsersMandatoryTaskToBeDone();
        Integer userTotalTasks = totalTasks.get(String.valueOf(id));
        Map<String, Integer> percentProgress = userService.findUsersPercentProgress();
        Integer userPercentProgress = percentProgress.get(String.valueOf(id));

        if (isUserReadyToBePromoted(userMandatoryTasksDone, userTotalTasks, userPercentProgress)) {
            notificationService.createPromoteNotification(new Notification(1, new User(id)));
            model.addAttribute("isReady", true);
            System.out.println("-----------------------------------------Yay");
        } else {
            model.addAttribute("isReady", false);
            System.out.println("-----------------------------------------Lame");
        }

        List<CareerLevel> all = careerLevelService.findAllCareerLevels();
        System.out.println(all);
        User user = userService.findById(id);
        Long nextCareerLevelId = user.getCareerLevel().getId();
        nextCareerLevelId++; // increment for next level
        if (nextCareerLevelId > 5)
            model.addAttribute("nextCareerLevelName", "heaven");
        else {
            CareerLevel nexCareerLevel = careerLevelService.findById(nextCareerLevelId);
            model.addAttribute("nextCareerLevelName", nexCareerLevel.getName());
        }

        model.addAttribute("careerLevels", careerLevelService.findAllCareerLevels());
        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());
        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }

        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        model.addAttribute("userId", id);
        model.addAttribute("username", userService.findById(id).getUsername());
        model.addAttribute("todos", todoService.findCurrentTodosByUserId(id));
        model.addAttribute(
                "finishedTodos", todoService.findCurrentFinishedTodosByUserId(id));
        model.addAttribute("user", userService.findById(id));
        model.addAttribute(
                "cards", responsibilityCardsService.findCurrentResponsibilityCardsByUserId(id));
        model.addAttribute(
                "availableCards", responsibilityCardsService.findAllAvailableResponsibilityCards());
        model.addAttribute(
                "careerLevel", careerLevelService.findCareerLevelByUserId(id));
        model.addAttribute("todoTypes", todoService.findAllTodoTypes());
        model.addAttribute("userMandatoryTasksDone", userMandatoryTasksDone);
        model.addAttribute("userTotalTasks", userTotalTasks);
        model.addAttribute("userPercentProgress", userPercentProgress);
        model.addAttribute("achievedPoints", todoService.findPointsOfFinishedTasksByUserId(id));
        model.addAttribute("totalPoints", todoService.findPointsToPassPerLevelByUserId(id));
        model.addAttribute("careerLevelTodoTypes", todoService.findAllTodoTypesByCareerLevelId(careerLevelId));
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext(); ) {
                    Object[] o = it.next();
                    if (seenEvents.contains(o[2])) {
                        it.remove();
                    }

                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }
        model.addAttribute("events", eventService.getAttendedEventByUserId(id));

        model.addAttribute("attenders", eventService.findAllAttenders());
        return "users/progress-page";
    }

    private boolean isUserReadyToBePromoted(Integer userMandatoryTasksDone, Integer userTotalTasks, Integer userPercentProgress) {
        return (userMandatoryTasksDone.compareTo(userTotalTasks) == 0) && (userPercentProgress.compareTo(100) >= 0);
    }

    /**
     * Prepares data for page with total progress for specific user.
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/users/total-progress/{id}")
    public String total_progress(@PathVariable("id") Long id, Model model) {
        //Get the current user
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }

        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());
        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }
        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        model.addAttribute("userId", id);
        model.addAttribute(
                "allFinishedTodos", todoService.findAllFinishedTodosByUserId(id));
        model.addAttribute("user", userService.findById(id));
        model.addAttribute(
                "careerLevel", careerLevelService.findCareerLevelByUserId(id));
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext(); ) {
                    Object[] o = it.next();
                    if (seenEvents.contains(o[2])) {
                        it.remove();
                    }

                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }
        return "users/total-progress";
    }

    /**
     * Increases level of user.
     *
     * @param userId Id of user.
     * @return
     */
    @RequestMapping(value = "/promote-user/{userId}", method = RequestMethod.POST)
    @Transactional
    public String promoteUser(@PathVariable("userId") Long userId,
                              @RequestParam("newCareerLevelId") Long newCareerLevelId) {
        try {
            System.out.println("---------------------------------------- " + newCareerLevelId);
            User user = userService.findById(userId);
            Long careerLevelId = null;
            if (newCareerLevelId.compareTo(-1L) == 0) {
                careerLevelId = user.getCareerLevel().getId();
                careerLevelId++; // increment for next level
            } else {
                careerLevelId = newCareerLevelId;
            }
            CareerLevel careerLevel = careerLevelService.findById(careerLevelId);

            if (careerLevel != null) {
                user.setCareerLevel(careerLevel);
                userService.edit(user);
                todoService.deleteTodoByUserId(userId);
                if (notificationService.findPromoteNotificationIdByUserId(userId) != null)
                    notificationService.deleteById(notificationService.findPromoteNotificationIdByUserId(userId).longValue());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/general-error?msg=" +
                    "Error promoting the user: " + ex.toString();

        }

        return "redirect:/users/progress-page/" + userId;
    }

    /**
     * /create  --> Create a new user and save it in the database.
     */
    @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public String createUser(
            String lastName, String firstName, String username, String password,
            String email, Long careerLevelId, Long groupId) {

        User user = null;
        try {
            CareerLevel careerLevel = careerLevelService.findById(careerLevelId);
            Group group = groupService.findById(groupId);

            BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
            String hashPwd = bc.encode(password);

            user = new User(lastName, firstName, username, hashPwd, email,
                    careerLevel, group);
            userService.create(user);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error creating the user: " + ex.toString();
        }
//        return "User succesfully created! (id = " + user.getId() + ")";
        return "redirect:/users/all-users";
    }

    /**
     * /update-user --> Update the user in the database.
     *
     * @param id            User id.
     * @param lastName      New last name.
     * @param firstName     New first name.
     * @param username      New username.
     * @param email         New email address.
     * @param careerLevelId New career level id.
     * @param groupId       New group id.
     * @return
     */
    @RequestMapping(value = "/update-user", method = RequestMethod.GET)
    public String updateUser(Long id, String lastName, String firstName,
                             String username, String email, Long careerLevelId,
                             Long groupId) {
        try {
            User user = userService.findById(id);
            user.setLastName(lastName);
            user.setFirstName(firstName);
            user.setUsername(username);
            user.setEmail(email);

            CareerLevel careerLevel = careerLevelService.findById(careerLevelId);
            user.setCareerLevel(careerLevel);

            Group group = groupService.findById(groupId);
            user.setGroup(group);

            userService.create(user);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error updating the user: " + ex.toString();
        }
        return "redirect:/users/all-users";
    }

    /**
     * /delete-user --> Delete the user having the passed id.
     *
     * @param id The id of the user to delete
     */
    @RequestMapping("/delete-user")
    @Transactional
    @Modifying
    public String deleteUser(Long id) {
        try {
            System.out.println(id);
            List<ResponsibilityCard> cardsByUserId = responsibilityCardsService.findResponsibilityCardsByUserId(id);
            if (!cardsByUserId.isEmpty()) {
                for (ResponsibilityCard r : cardsByUserId) {
                    responsibilityCardsService.updateResponsibilityCardByCardId(r.getId());
                }
            }
            assignmentService.deleteAssignmentsByUserId(id);
            notificationService.deleteNotificationByUserId(id);

            List<Event> allEventsCreatedByUserId = eventService.findAllEventsCreatedByUserId(id);
            if (!allEventsCreatedByUserId.isEmpty()) {
                for (Event e : allEventsCreatedByUserId) {
                    eventService.deleteEventSeenByEventId(e.getId());
                    eventService.deleteEventCommentByEventId(e.getId());
                    eventService.deleteEventAttendByEventId(e.getId());
                    notificationService.deleteEventNotificationByEventId(e.getId());
                }
            }
            eventService.deleteEventAttendByUserId(id);
            eventService.deleteEventCommentByUserId(id);
            eventService.deleteEventSeenByUserId(id);
            eventService.deleteEventByUserId(id);
            userService.deleteById(id);
            System.out.println("yay");
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error deleting the user by id=" + id + ": " + ex.toString();
        }
        return "redirect:/users/all-users";
    }

    /**
     * /get-user-by-id --> Return information about user using his id.
     *
     * @param id The id to search in the database.
     * @return A message with information about the user or a message error if
     * the user is not found.
     */
    @RequestMapping("/get-user-by-id")
    @ResponseBody
    public String getUserById(long id) {
        User user = null;
        try {
            user = userService.findById(id);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "User not found";
        }
        return "id: " + user.getId() + ", last name: " + user.getLastName() + ", first name: " + user.getFirstName()
                + ", career level: " + user.getCareerLevel();
    }

} // class UserController
