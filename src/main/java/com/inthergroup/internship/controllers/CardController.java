package com.inthergroup.internship.controllers;

import com.inthergroup.internship.models.*;
import com.inthergroup.internship.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Created by Hiruine on 07/03/17.
 */

@Controller
public class CardController {

    @Autowired
    private UserService userService;

    @Autowired
    private ResponsibilityCardsService responsibilityCardsService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private AssignmentService assignmentService;
    @Autowired
    private EventService eventService;
    @Autowired
    private ServletContext servletContext;

    @RequestMapping("/cards/all-cards")
    public String cards(Model model) {
        //Get the current user
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }
        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }
        model.addAttribute("allUsers", userService.findAll());
        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());
        model.addAttribute("cards", responsibilityCardsService.findAllResponsibilityCards());
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext();) {
                    Object[] o = it.next();
                    if (seenEvents.contains(o[2])){
                        it.remove();
                    }

                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }
        return "/cards/all-cards";
    }

    @RequestMapping("/cards/history-cards")
    public String history(Model model) {
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }
        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }
        model.addAttribute("allUsers", userService.findAll());
        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());
        model.addAttribute("history", assignmentService.findAllAssignments());
        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext();) {
                    Object[] o = it.next();
                    if (seenEvents.contains(o[2])){
                        it.remove();
                    }

                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }
        return "/cards/history-cards";
    }


    @RequestMapping(value = "/create-responsibility-card", headers = ("content-type=multipart/*"), consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String createResponsibilityCard(String type, String description, String expectedResult,
                                           @RequestParam("file") MultipartFile file) {

        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
        }
        ResponsibilityCard responsibilityCard = null;
        Random rn = new Random();
        int randomNum = rn.nextInt(17) + 1;
        StringBuilder sb = new StringBuilder();
        sb.append("/images/m");
        sb.append(randomNum);
        sb.append(".png");

        try {
            if (!file.isEmpty()) {
                sb = new StringBuilder("/images/" + file.getOriginalFilename());
                BufferedImage src = ImageIO.read(new ByteArrayInputStream(file.getBytes()));
                String path = (servletContext.getRealPath("images/") + file.getOriginalFilename());
                System.out.println(path);
                if (path.contains("file:/"))
                    path = path.replace("file:/", "");
                File destination = new File(path);
                ImageIO.write(src, "png", destination);
            }

            responsibilityCard = new ResponsibilityCard(sb.toString(), description, expectedResult, type, userService.findByUsername(currentUserName));
            responsibilityCardsService.create(responsibilityCard);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/general-error?msg=" +
                    "Error creating the responsibilityCard: " + ex.toString();
        }
        return "redirect:/cards/all-cards";
    }

    @RequestMapping("/update-responsibility-card")
    public String updateResponsibilityCard(long responsibilityCardId, String typeUpdate, String descriptionUpdate, String expectedResultUpdate) {
        ResponsibilityCard responsibilityCard = null;
        System.out.println("RESP CARD ID: " + responsibilityCardId);
        try {
            responsibilityCard = responsibilityCardsService.findById(responsibilityCardId);
            responsibilityCard.setType(typeUpdate);
            responsibilityCard.setDescription(descriptionUpdate);
            responsibilityCard.setExpectedResult(expectedResultUpdate);
            responsibilityCardsService.edit(responsibilityCard);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error creating the Benefit Type: " + ex.toString();
        }
        return "redirect:/cards/all-cards";
    }

    @RequestMapping("/assign-responsibility-card")
    public String assignResponsibilityCard(long responsibilityCardIdAssign, long newUserId) {
        ResponsibilityCard responsibilityCard = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        try {
            responsibilityCard = responsibilityCardsService.findById(responsibilityCardIdAssign);
            User u = new User(newUserId);
            responsibilityCard.setUser(u);
            responsibilityCardsService.edit(responsibilityCard);
            assignmentService.createAssignment(new Assignment(responsibilityCard, u, new Date()));

            Integer reqCardNotification = notificationService.findRequestNotificationIdByUserIdAndReqCardId(newUserId, responsibilityCardIdAssign);
            if(reqCardNotification != null){
                notificationService.deleteById(reqCardNotification.longValue());
                System.out.println("yay");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return "redirect:/general-error?msg=" +
                    "Error creating the resp card: " + ex.toString();

        }
        return "redirect:/cards/all-cards";
    }

    @RequestMapping("/deassign-responsibility-card")
    @Transactional
    public String deasignResponsibilityCard(@RequestParam("responsibilityCardIdDeassign") Long cardId){
        System.out.println(cardId);
            responsibilityCardsService.updateResponsibilityCardByCardId(cardId);
        return "redirect:/cards/all-cards";
    }


    @RequestMapping("/remove-responsibility-card")
    @Transactional
    public String deleteResponsibilityCard(Long id) {
        try {
            System.out.println(id);
            responsibilityCardsService.deleteById(id);
        } catch (Exception ex) {
            return "redirect:/general-error?msg=" +
                    "Error deleting the resp card by id #" + id + ": " + ex.toString();
        }
        return "redirect:/cards/all-cards";
    }


    @RequestMapping(value = "/add-request-resp-card/{id}", method = RequestMethod.POST)
    @Transactional
    public String addTodoToUser(
            @PathVariable("id") Long id, @RequestParam("reqCardId") Long reqCardId){
        System.out.println(id);
        System.out.println(reqCardId);
        ResponsibilityCard responsibilityCard = responsibilityCardsService.findById(reqCardId);
        System.out.println(responsibilityCard);
        Integer reqCardNotification = notificationService.findRequestNotificationIdByUserIdAndReqCardId(id, reqCardId);
        if(reqCardNotification == null) {
            notificationService.createRequestNotificationFromParams(id, reqCardId);
        }
        return "redirect:/users/progress-page/" + id;
    }
}

