package com.inthergroup.internship.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.Notification;
import com.inthergroup.internship.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.inthergroup.internship.models.CareerLevel;
import com.inthergroup.internship.models.User;

@Controller
public class HomeController {

    @Autowired
    private CareerLevelService careerLevelService;

    @Autowired
    private BenefitService benefitService;

    @Autowired
    private UserService userService;

    @Autowired
    private TodoService todoService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private EventService eventService;

    /**
     * Prepares data for main page.
     *
     * @param model
     * @return
     */
    @RequestMapping({"/", "/index"})
    public String index(Model model) {

        System.out.println(eventService.findAllAvailableEvents());

        //Get the current user
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            model.addAttribute("currentUser", user);
        }

        // List all career levels
        List<CareerLevel> careerLevels = careerLevelService.findAllByOrderByIdAsc();
        model.addAttribute("careerLevels", careerLevels);


        // For each career level list its benefits

        List<List<Object[]>> benefits = new ArrayList<List<Object[]>>();
        for (long i = 1; i <= careerLevels.size(); i++) {
            List<Object[]> benefitsFromLevel = benefitService.findBenefitsFromLevel(i);
            benefits.add(benefitsFromLevel);
        }
        model.addAttribute("benefits", benefits);

        // For each career level list its users
        List<List<User>> users = new ArrayList<List<User>>();
        for (long i = 1; i <= careerLevels.size(); i++) {
            List<User> usersFromLevel = userService.findUsersFromLevel(i);
            users.add(usersFromLevel);
        }
        model.addAttribute("users", users);

        // For each career level list its todos
        List<List<Object[]>> todos = new ArrayList<List<Object[]>>();
        for (long i = 1; i <= careerLevels.size(); i++) {
            List<Object[]> todosFromLevel = todoService.findTodosFromLevel(i);
            todos.add(todosFromLevel);
        }
        model.addAttribute("todos", todos);

        // For each career level list its todos
        List<List<Object[]>> mandatoryTodos = new ArrayList<List<Object[]>>();
        for (long i = 1; i <= careerLevels.size(); i++) {
            List<Object[]> todosFromLevel = todoService.findMandatoryTodosFromLevel(i);
            mandatoryTodos.add(todosFromLevel);
        }
        model.addAttribute("mTodos", mandatoryTodos);

        // For each career level list its todos
        List<List<Object[]>> optionalTodos = new ArrayList<List<Object[]>>();
        for (long i = 1; i <= careerLevels.size(); i++) {
            List<Object[]> todosFromLevel = todoService.findOptionalTodosFromLevel(i);
            optionalTodos.add(todosFromLevel);
        }
        model.addAttribute("oTodos", optionalTodos);

        // Map with percent progress for corresponding user id.
        model.addAttribute(
                "usersPercentProgress", userService.findUsersPercentProgress());

        model.addAttribute("userTaskDone", userService.findUsersMandatoryTaskDone());
        model.addAttribute("userTaskToBeDone", userService.findUsersMandatoryTaskToBeDone());

        List<Object[]> allPromoteNotifications = notificationService.findAllPromoteNotifications();
        for (Object o : allPromoteNotifications) {
            System.out.println(o);
        }
        model.addAttribute("notifications", notificationService.findAllPromoteNotifications());
        List<Object[]> allRequestNotifications = notificationService.findAllRequestNotifications();
        for (Object o : allRequestNotifications) {
            System.out.println(o);
        }
        model.addAttribute("reqNotifications", notificationService.findAllRequestNotifications());


        authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
            User user = userService.findByUsername(currentUserName);
            List<Object[]> allEventNotifications = notificationService.findAllEventNotifications();
            List<Event> seenEvents = eventService.findEventsSeenByUserId(user.getId());
            System.out.println(allEventNotifications);
            if (!seenEvents.isEmpty()) {
                for (Iterator<Object[]> it = allEventNotifications.iterator(); it.hasNext();) {
                    Object[] o = it.next();
                        if (seenEvents.contains(o[2])){
                            it.remove();
                        }

                }
            }
            model.addAttribute("eventNotifications", allEventNotifications);
        }

        model.addAttribute("mapOfPointsToPass", todoService.findPointsToPassPerLevelOfAllUsers());
        model.addAttribute("mapOfPointsOfFinishedTasks", todoService.findPointsOfFinishedTasksOfAllUsers());
        return "index";
    }
} // class HomeController
