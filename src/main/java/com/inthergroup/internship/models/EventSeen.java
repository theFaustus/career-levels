package com.inthergroup.internship.models;

import javax.persistence.*;

/**
 * Created by Faust on 7/6/2017.
 */

@Entity
@Table(name = "event_seen")
public class EventSeen {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(length = 20)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User attender;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "event_id")
    private Event event;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAttender() {
        return attender;
    }

    public void setAttender(User attender) {
        this.attender = attender;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventSeen that = (EventSeen) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getAttender() != null ? !getAttender().equals(that.getAttender()) : that.getAttender() != null)
            return false;
        return getEvent() != null ? getEvent().equals(that.getEvent()) : that.getEvent() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getAttender() != null ? getAttender().hashCode() : 0);
        result = 31 * result + (getEvent() != null ? getEvent().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventAttend{" +
                "id=" + id +
                ", attender=" + attender +
                ", event=" + event +
                '}';
    }
}
