package com.inthergroup.internship.models;

import javax.persistence.*;

/**
 * Created by dummy on 03/07/2017.
 */

@Entity
@Table(name="responsibility_cards")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.user",
                joinColumns = @JoinColumn(name = "user_id", nullable = false))})
public class ResponsibilityCard {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private long id;

    @Column(name = "image_path")
    private String imagePath;

    @Column(name = "description")
    private String description;

    @Column(name = "expected_result")
    private String expectedResult;

    @Column(name = "type")
    private String type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public ResponsibilityCard(String imagePath, String description, String expectedResult, String type, User user) {

        this.imagePath = imagePath;
        this.description = description;
        this.expectedResult = expectedResult;
        this.type = type;
        this.user = user;
    }

    public ResponsibilityCard(long id) {
        this.id = id;
        this.user = new User();
    }

    public ResponsibilityCard(ResponsibilityCard rc, User u){
        this.id = rc.getId();
        this.imagePath = rc.getImagePath();
        this.description = rc.getDescription();
        this.expectedResult = rc.getExpectedResult();
        this.type = rc.getType();
        this.user = u;
    }

    public ResponsibilityCard(ResponsibilityCard rc, long id){
        this.id = rc.getId();
        this.imagePath = rc.getImagePath();
        this.description = rc.getDescription();
        this.expectedResult = rc.getExpectedResult();
        this.type = rc.getType();
        this.user = new User(id);
    }


    public ResponsibilityCard() {
        this.user = new User();
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addUser(Long id){
        this.user = new User(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResponsibilityCard)) return false;

        ResponsibilityCard that = (ResponsibilityCard) o;

        if (getId() != that.getId()) return false;
        if (getImagePath() != null ? !getImagePath().equals(that.getImagePath()) : that.getImagePath() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(that.getDescription()) : that.getDescription() != null)
            return false;
        if (getExpectedResult() != null ? !getExpectedResult().equals(that.getExpectedResult()) : that.getExpectedResult() != null)
            return false;
        if (getType() != null ? !getType().equals(that.getType()) : that.getType() != null) return false;
        return getUser() != null ? getUser().equals(that.getUser()) : that.getUser() == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getImagePath() != null ? getImagePath().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getExpectedResult() != null ? getExpectedResult().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ResponsibilityCard{" +
                "id=" + id +

                ", imagePath='" + imagePath + '\'' +
                ", description='" + description + '\'' +
                ", expectedResult='" + expectedResult + '\'' +
                ", type='" + type + '\'' +
                ", user=" + user +
                '}';
    }
}
