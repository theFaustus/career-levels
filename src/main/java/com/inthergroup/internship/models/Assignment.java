package com.inthergroup.internship.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Faust on 7/3/2017.
 */
@Entity
@Table(name = "assignments")
@AssociationOverrides({
        @AssociationOverride(name = "primaryKey.user", joinColumns = @JoinColumn(name = "user_id", nullable = false)),
        @AssociationOverride(name = "primaryKey.card", joinColumns = @JoinColumn(name = "user_id", nullable = false))})
public class Assignment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 20)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "card_id", nullable = false)
    private ResponsibilityCard card;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @JoinColumn(name = "assign_date", nullable = false)
    private Date assignDate;

    public Assignment() {
        user = new User();
        card = new ResponsibilityCard();
        assignDate = new Date();
    }

    public Assignment(ResponsibilityCard card, User user, Date assignDate) {
        this.card = card;
        this.user = user;
        this.assignDate = assignDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ResponsibilityCard getCard() {
        return card;
    }

    public void setCard(ResponsibilityCard card) {
        this.card = card;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(Date assignDate) {
        this.assignDate = assignDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Assignment that = (Assignment) o;

        if (getId() != that.getId()) return false;
        if (getCard() != null ? !getCard().equals(that.getCard()) : that.getCard() != null) return false;
        if (getUser() != null ? !getUser().equals(that.getUser()) : that.getUser() != null) return false;
        return getAssignDate() != null ? getAssignDate().equals(that.getAssignDate()) : that.getAssignDate() == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getCard() != null ? getCard().hashCode() : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        result = 31 * result + (getAssignDate() != null ? getAssignDate().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "id=" + id +
                ", card=" + card +
                ", user=" + user +
                ", assignDate=" + assignDate +
                '}';
    }
}
