package com.inthergroup.internship.models;

import javax.persistence.*;

/**
 * Created by Faust on 7/3/2017.
 */
@Entity
@Table(name = "notification")
@AssociationOverride(name = "user", joinColumns = @JoinColumn(name = "user_id", nullable = false))
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 20)
    private long id;

    @Column(nullable = false)
    private Integer type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, unique = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "req_card_id", columnDefinition = "int default null", nullable = true, unique = false)
    private ResponsibilityCard card;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", columnDefinition = "int default null", nullable = true, unique = false)
    private Event event;

    public Notification() {
        user = new User();
    }

    public Notification(Integer type, User user) {
        this.type = type;
        this.user = user;
    }

    public Notification(Integer type, User user, ResponsibilityCard card) {
        this.type = type;
        this.user = user;
        this.card = card;
    }

    public Notification(Integer type, User user, Event event) {
        this.type = type;
        this.user = user;
        this.card = new ResponsibilityCard();
        this.event = event;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void addUser(Long id) {
        this.user = new User(id);
    }

    public ResponsibilityCard getCard() {
        return card;
    }

    public void setCard(ResponsibilityCard card) {
        this.card = card;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notification that = (Notification) o;

        if (getId() != that.getId()) return false;
        if (getType() != null ? !getType().equals(that.getType()) : that.getType() != null) return false;
        if (getUser() != null ? !getUser().equals(that.getUser()) : that.getUser() != null) return false;
        if (getCard() != null ? !getCard().equals(that.getCard()) : that.getCard() != null) return false;
        return getEvent() != null ? getEvent().equals(that.getEvent()) : that.getEvent() == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (getId() ^ (getId() >>> 32));
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getUser() != null ? getUser().hashCode() : 0);
        result = 31 * result + (getCard() != null ? getCard().hashCode() : 0);
        result = 31 * result + (getEvent() != null ? getEvent().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", type=" + type +
                ", user=" + user +
                ", card=" + card +
                ", event=" + event +
                '}';
    }
}
