package com.inthergroup.internship.models;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Faust on 7/6/2017.
 */

@Entity
@Table(name = "event_attend")
public class EventAttend {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(length = 20)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User attender;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "event_id")
    private Event event;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getAttender() {
        return attender;
    }

    public void setAttender(User attender) {
        this.attender = attender;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public EventAttend(User attender, Event event) {
        this.attender = attender;
        this.event = event;
    }

    public EventAttend() {
        this.attender = new User();
        this.event = new Event();
    }

    public EventAttend(Long userId, Long eventId) {
        this.attender = new User(userId);
        this.event = new Event(eventId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventAttend that = (EventAttend) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getAttender() != null ? !getAttender().equals(that.getAttender()) : that.getAttender() != null)
            return false;
        return getEvent() != null ? getEvent().equals(that.getEvent()) : that.getEvent() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getAttender() != null ? getAttender().hashCode() : 0);
        result = 31 * result + (getEvent() != null ? getEvent().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EventAttend{" +
                "id=" + id +
                ", attender=" + attender +
                ", event=" + event +
                '}';
    }
}
