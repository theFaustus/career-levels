package com.inthergroup.internship.models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Faust on 7/6/2017.
 */

@Entity
@Table(name = "events")
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(length = 20)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "active")
    private Long active;

    @Column(name = "date")
    private Date date;

    @Column(name = "place")
    private String place;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private User userAuthor;

    @Column(name = "author")
    private Long author;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getActive() {
        return active;
    }

    public void setActive(Long active) {
        this.active = active;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public User getUserAuthor() {
        return userAuthor;
    }

    public void setUserAuthor(User userAuthor) {
        this.userAuthor = userAuthor;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public Event(String name, String description, Long active, Date date, String place, User userAuthor, Long author) {
        this.name = name;
        this.description = description;
        this.active = active;
        this.date = date;
        this.place = place;
        this.userAuthor = userAuthor;
        this.author = author;
    }

    public Event() {
        this.userAuthor = new User();
    }

    public Event(Event e) {
        this.name = e.getName();
        this.description = e.getDescription();
        this.active = e.getActive();
        this.date = e.getDate();
        this.place = e.getPlace();
        this.userAuthor = e.getUserAuthor();
        this.author = e.getAuthor();
    }

    public Event(Long userId){
        this.userAuthor = new User(userId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (getId() != null ? !getId().equals(event.getId()) : event.getId() != null) return false;
        if (getName() != null ? !getName().equals(event.getName()) : event.getName() != null) return false;
        if (getDescription() != null ? !getDescription().equals(event.getDescription()) : event.getDescription() != null)
            return false;
        if (getActive() != null ? !getActive().equals(event.getActive()) : event.getActive() != null) return false;
        if (getDate() != null ? !getDate().equals(event.getDate()) : event.getDate() != null) return false;
        if (getPlace() != null ? !getPlace().equals(event.getPlace()) : event.getPlace() != null) return false;
        if (getUserAuthor() != null ? !getUserAuthor().equals(event.getUserAuthor()) : event.getUserAuthor() != null)
            return false;
        return getAuthor() != null ? getAuthor().equals(event.getAuthor()) : event.getAuthor() == null;

    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getActive() != null ? getActive().hashCode() : 0);
        result = 31 * result + (getDate() != null ? getDate().hashCode() : 0);
        result = 31 * result + (getPlace() != null ? getPlace().hashCode() : 0);
        result = 31 * result + (getUserAuthor() != null ? getUserAuthor().hashCode() : 0);
        result = 31 * result + (getAuthor() != null ? getAuthor().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", active=" + active +
                ", date=" + date +
                ", place='" + place + '\'' +
                ", userAuthor=" + userAuthor +
                ", author=" + author +
                '}';
    }
}
