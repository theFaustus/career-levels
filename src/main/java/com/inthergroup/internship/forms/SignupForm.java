package com.inthergroup.internship.forms;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class SignupForm {
    @NotEmpty
    @Size(min=4, max=30)
    private String firstName = "";
    
    @NotEmpty
    @Size(min=4, max=30)
    private String lastName = "";
    
    @NotEmpty
    @Pattern(regexp = "^[_A-Za-z0-9-\\\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    @Size(min=4, max=30)
    private String email = "";
    
    @NotEmpty
    @Size(min=4, max=30)
    private String username = "";
    
    @NotEmpty
    @Pattern(regexp = "(?=^.{8,}$)(?=.*[0-9a-z0-9]).*$")
    @Size(min=8, max=30)
    private String password = "";
    
    @NotEmpty
    @Pattern(regexp = "(?=^.{8,}$)(?=.*[0-9a-z0-9]).*$")
    @Size(min=8, max=30)
    private String passwordCheck = "";
    
    @NotEmpty
    private String role = "USER";

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordCheck() {
        return passwordCheck;
    }

    public void setPasswordCheck(String passwordCheck) {
        this.passwordCheck = passwordCheck;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
