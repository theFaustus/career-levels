package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.EventAttend;
import com.inthergroup.internship.models.User;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Faust on 7/6/2017.
 */

public interface EventService {

    List<Event> findAllAvailableEvents();

    Event findAvailableEventByEventId(Long eventId);

    List<Object[]> findAllAttenders();

    Event save(Event event);

    void deleteById(Long id);

    Event findById(Long id);

    List<EventAttend> findAllEventAttends();

    Event edit(Event rc);

    void deleteEventAttendByEventId(Long eventId);

    void deleteEventAttendByEventIdAndByUserId(Long eventId, Long userId);

    void deleteEventCommentByEventId(Long eventId);

    void updateEventCommentByCommentId(Long commentId, String text);

    void deleteEventSeenByEventId(Long eventId);

    void deleteEventCommentByEventIdAndUserId(Long eventId, Long userId);

    void deleteEventCommentByUserId(Long userId);

    void deleteAllAboutEventsByUserId(Long userId);

    void createEventAttend(Long eventId, Long userId);

    void createEventComment(Long eventId, Long userId, String text);

    void createEventSeen(Long eventId, Long userId);

    List<Event> findEventsSeenByUserId(Long id);

    Integer findEventAttendByUserIdAndEventId(Long userId, Long eventId);

    Integer findEventSeenByUserIdAndEventId(Long userId, Long eventId);

    Integer findEventCommentByUserIdAndEventId(Long userId, Long eventId);

    List<Event> getAttendedEventByUserId(@Param("userId") Long userId);

    void deleteEventAttendByUserId(Long userId);

    void deleteEventByUserId(Long userId);

    void deleteEventSeenByUserId(Long userId);

    List<Event> findAllEventsCreatedByUserId(Long userId);


}
