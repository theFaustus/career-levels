package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.EventAttend;
import com.inthergroup.internship.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Faust on 7/6/2017.
 */
@Primary
@Service
public class EventServiceJpaImpl implements EventService {
    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<Event> findAllAvailableEvents() {
        return eventRepository.findAllAvailableEvents();
    }

    @Override
    public Event findAvailableEventByEventId(Long eventId) {
        return eventRepository.findAvailableEventByEventId(eventId);
    }

    @Override
    public List<Object[]> findAllAttenders() {
        return eventRepository.findAllAttenders();
    }

    @Override
    public void deleteById(Long id) {
        this.eventRepository.delete(id);

    }

    @Override
    public List<EventAttend> findAllEventAttends() {
        return eventRepository.findAllEventAttends();
    }

    @Override
    public void deleteEventAttendByEventId(Long eventId) {
        eventRepository.deleteEventAttendByEventId(eventId);
    }

    @Override
    public Event save(Event event) {
        return eventRepository.save(event);
    }

    @Override
    public Event findById(Long id) {
        return this.eventRepository.findOne(id);
    }

    @Override
    public Event edit(Event event) {
        return this.eventRepository.save(event);
    }

    @Override
    public void createEventAttend(Long eventId, Long userId) {
        this.eventRepository.createEventAttend(eventId, userId);
    }

    @Override
    public void createEventSeen(Long eventId, Long userId) {
        this.eventRepository.createEventSeen(eventId, userId);
    }

    @Override
    public List<Event> findEventsSeenByUserId(Long id) {
        return eventRepository.findEventsSeenByUserId(id);
    }

    @Override
    public Integer findEventAttendByUserIdAndEventId(Long userId, Long eventId) {
        return eventRepository.findEventAttendByUserIdAndEventId(userId, eventId);
    }

    @Override
    public Integer findEventSeenByUserIdAndEventId(Long userId, Long eventId) {
        return eventRepository.findEventSeenByUserIdAndEventId(userId, eventId);
    }

    @Override
    public List<Event> getAttendedEventByUserId(@Param("userId") Long userId){
        return this.eventRepository.getAttendedEventByUserId(userId);
    }

    @Override
    public void deleteEventCommentByEventId(Long eventId) {
        this.eventRepository.deleteEventCommentByEventId(eventId);
    }

    @Override
    public void updateEventCommentByCommentId(Long commentId, String text) {
        this.eventRepository.updateEventCommentByCommentId(commentId, text);
    }

    @Override
    public void deleteEventCommentByUserId(Long userId) {
        this.eventRepository.deleteEventCommentByUserId(userId);
    }

    @Override
    public void createEventComment(Long eventId, Long userId, String text) {
        this.eventRepository.createEventComment(eventId, userId, text);
    }

    @Override
    public Integer findEventCommentByUserIdAndEventId(Long userId, Long eventId) {
        return this.eventRepository.findEventCommentByUserIdAndEventId(userId, eventId);
    }

    @Override
    public void deleteEventCommentByEventIdAndUserId(Long eventId, Long userId) {
        this.eventRepository.deleteEventCommentByEventIdAndUserId(eventId, userId);
    }

    @Override
    public void deleteEventAttendByEventIdAndByUserId(Long eventId, Long userId) {
        this.eventRepository.deleteEventAttendByEventIdAndByUserId(eventId, userId);
    }

    @Override
    public void deleteEventSeenByEventId(Long eventId) {
        this.eventRepository.deleteEventSeenByEventId(eventId);
    }

    @Override
    public void deleteAllAboutEventsByUserId(Long userId) {
    }

    @Override
    public void deleteEventAttendByUserId(Long userId) {
        eventRepository.deleteEventAttendByUserId(userId);
    }

    @Override
    public void deleteEventByUserId(Long userId) {
        eventRepository.deleteEventByUserId(userId);
    }

    @Override
    public void deleteEventSeenByUserId(Long userId) {
        eventRepository.deleteEventSeenByUserId(userId);
    }

    @Override
    public List<Event> findAllEventsCreatedByUserId(Long userId) {
        return eventRepository.findAllEventsCreatedByUserId(userId);
    }
}
