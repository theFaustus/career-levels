package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.Notification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Faust on 7/3/2017.
 */
public interface NotificationService {
    List<Object[]> findAllPromoteNotifications();

    Notification createPromoteNotification(Notification n);

    Integer findPromoteNotificationIdByUserId(Long id);

    List<Object[]> findAllRequestNotifications();

    Notification createRequestNotification(Notification n);

    void createRequestNotificationFromParams(Long userId, Long reqCardId);

    Integer findRequestNotificationIdByUserId(Long id);

    Integer findRequestNotificationIdByUserIdAndReqCardId(Long userId, Long reqCardId);

    Notification edit(Notification n);

    void deleteById(Long id);

    List<Object[]> findAllEventNotifications();

    Integer findEventNotificationByUserId(Long id);

    Integer findEventNotificationByUserIdAndEventId(Long id, Long eventId );

    void createEventNotificationFromParams(Long userId, Long eventId);

    void deleteEventNotificationByEventId(Long eventId);

    void deleteNotificationByUserId(Long userId);


}
