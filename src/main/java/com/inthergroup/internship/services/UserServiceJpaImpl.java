package com.inthergroup.internship.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.inthergroup.internship.models.User;
import com.inthergroup.internship.repositories.TodoRepository;
import com.inthergroup.internship.repositories.UserRepository;

@Service
@Primary
public class UserServiceJpaImpl implements UserService {
    @Autowired
    private UserRepository userRepo;
    
    @Autowired
    private TodoRepository todoRepo;

    @Override
    public List<User> findAll() {
        return this.userRepo.findAll();
    }

    @Override
    public User findById(Long id) {
        return this.userRepo.findOne(id);
    }
    
    @Override
    public User findByUsername(String username) {
        return this.userRepo.findByUsername(username);
    }

    @Override
    public User create(User user) {
        System.out.println(user);
        return this.userRepo.save(user);
    }

    @Override
    public User edit(User user) {
        return this.userRepo.save(user);
    }

    @Override
    public void deleteById(Long id) {
        this.userRepo.delete(id);
    }
    
    @Override
    public List<User> findUsersFromLevel(Long id) {
        return userRepo.findByCareerLevelId(id);
    }

    /**
     * For each user find progress in percent.
     * 
     * @return
     */
    @Override
    public Map<String, Integer> findUsersPercentProgress() {
        // Map consisting of user id (String) and appropriate percent progress (Integer) 
        HashMap<String, Integer> usersPercentProgress = new HashMap<>();



        // array of all user ids (ordered by asc)
        long[] userIds = new long[userRepo.findNumberOfUserIds()];
        userIds = userRepo.findUserIds();

        List<Integer> accumulatedPointsPerUser = new ArrayList<>();
        List<Integer> pointsToPassPerUser = new ArrayList<>();
        for(int i = 0; i < userIds.length; i++){
            List<Integer> points = todoRepo.findPointsOfFinishedTasksByUserId(userIds[i]);
            Integer sum = 0;
            for(Integer p : points){
                sum += p;
                System.out.println(userIds[i] + " " + p);
            }
            accumulatedPointsPerUser.add(sum);
            pointsToPassPerUser.add(todoRepo.findPointsToPassPerLevelByUserId(userIds[i]));
        }
        System.out.println(accumulatedPointsPerUser);
        System.out.println(pointsToPassPerUser);
        
        // Find percent progress for each user
        for (int i = 0; i < pointsToPassPerUser.size(); i++) {
            double percent =
                    (double) accumulatedPointsPerUser.get(i) /
                    (double) pointsToPassPerUser.get(i) * 100;
            usersPercentProgress.put(String.valueOf(userIds[i]), (int) percent);
        }
        
        return usersPercentProgress;
    }

    @Override
    public Map<String, Integer> findUsersMandatoryTaskDone() {
        long[] userIds = new long[userRepo.findNumberOfUserIds()];
        userIds = userRepo.findUserIds();

        Map<String, Integer> result = new HashMap<>();

        for(int i = 0; i < userIds.length; i++){
            List<Integer> tasks = todoRepo.findAllMandatoryTasksAUserMadeByUserId(userIds[i]);
            result.put(String.valueOf(userIds[i]), tasks.size());
        }
        return result;
    }

    @Override
    public Map<String, Integer> findUsersMandatoryTaskToBeDone() {
        long[] userIds = new long[userRepo.findNumberOfUserIds()];
        userIds = userRepo.findUserIds();

        Map<String,  Integer> result = new HashMap<>();

        for(int i = 0; i < userIds.length; i++){
            List<Integer> tasksToBeDone = todoRepo.findMandatoryTasksPerLevelByUserId(userIds[i]);
            result.put(String.valueOf(userIds[i]), tasksToBeDone.size());
        }
        return result;
    }


    @Override
    public boolean isUserEligibleToBePromoted(Long id) {
        return false;
    }
}
