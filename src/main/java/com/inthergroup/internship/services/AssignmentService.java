package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Assignment;
import com.inthergroup.internship.models.Notification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Faust on 7/5/2017.
 */

public interface AssignmentService {
    List<Object[]> findAllAssignments();

    Assignment createAssignment(Assignment n);

    Integer findAssignmentIdByUserId(Long id);

    Assignment edit(Assignment n);

    void deleteById(Long id);

    void deleteAssignmentsByUserId(Long userId);

}
