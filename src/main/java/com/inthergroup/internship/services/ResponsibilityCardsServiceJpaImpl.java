package com.inthergroup.internship.services;

import com.inthergroup.internship.models.ResponsibilityCard;
import com.inthergroup.internship.models.User;
import com.inthergroup.internship.repositories.ResponsibilityCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dummy on 04/07/2017.
 */
@Service
@Primary
public class ResponsibilityCardsServiceJpaImpl implements ResponsibilityCardsService{

    @Autowired
    private ResponsibilityCardRepository responsibilityCardRepository;

    @Override
    public List<ResponsibilityCard> findAllResponsibilityCards() {
        return responsibilityCardRepository.findAllResponsibilityCards();
    }
    @Override
    public List<Object[]> findCurrentResponsibilityCardsByUserId(Long id) {
        return this.responsibilityCardRepository.findCurrentResponsibilityCardsByUserId(id);
    }

    @Override
    public ResponsibilityCard create(ResponsibilityCard responsibilityCard) {
        return this.responsibilityCardRepository.save(responsibilityCard);
    }

    @Override
    public ResponsibilityCard edit(ResponsibilityCard responsibilityCard) {
        return this.responsibilityCardRepository.save(responsibilityCard);
    }

    @Override
    public ResponsibilityCard findById(Long id) {
        return this.responsibilityCardRepository.findOne(id);
    }

    @Override
    public void deleteById(Long id) {
        this.responsibilityCardRepository.delete(id);
    }

    @Override
    public List<ResponsibilityCard> findAllAvailableResponsibilityCards() {
        return this.responsibilityCardRepository.findAllAvailableResponsibilityCards();
    }

    @Override
    public void updateResponsibilityCardByCardId(Long id) {
        this.responsibilityCardRepository.updateResponsibilityCardUser(id);
    }

    @Override
    public List<ResponsibilityCard> findResponsibilityCardsByUserId(Long id) {
        return responsibilityCardRepository.findResponsibilityCardsByUserId(id);
    }
}
