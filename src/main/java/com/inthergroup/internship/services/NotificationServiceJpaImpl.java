package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.Notification;
import com.inthergroup.internship.repositories.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Faust on 7/3/2017.
 */
@Service
@Primary
public class NotificationServiceJpaImpl implements NotificationService {

    @Autowired
    private NotificationRepository notifRepo;


    @Override
    public List<Object[]> findAllPromoteNotifications() {
        return notifRepo.findAllPromoteNotifications();
    }

    @Override
    public Notification createPromoteNotification(Notification n) {
        Integer promoteNotifications = notifRepo.findPromoteNotificationByUserId(n.getUser().getId());
        if (promoteNotifications == null)
            return notifRepo.save(n);
        return n;
    }

    @Override
    public Integer findPromoteNotificationIdByUserId(Long id) {
        return notifRepo.findPromoteNotificationByUserId(id);
    }


    @Override
    public Notification edit(Notification n) {
        return notifRepo.save(n);
    }

    @Override
    public void deleteById(Long id) {
        this.notifRepo.delete(id);
    }

    @Override
    public List<Object[]> findAllRequestNotifications() {
        return notifRepo.findAllRequestNotifications();
    }

    @Override
    public Notification createRequestNotification(Notification n) {
        return null;
    }

    @Override
    public Integer findRequestNotificationIdByUserId(Long id) {
        return notifRepo.findRequestNotificationByUserId(id);
    }

    @Override
    public void createRequestNotificationFromParams(Long userId, Long reqCardId) {
        notifRepo.createRequestNotificationFromParams(userId, reqCardId);
    }

    @Override
    public Integer findRequestNotificationIdByUserIdAndReqCardId(Long userId, Long reqCardId) {
        return notifRepo.findRequestNotificationByUserIdAndReqCardId(userId, reqCardId);
    }

    @Override
    public List<Object[]> findAllEventNotifications() {
        return notifRepo.findAllEventNotifications();
    }

    @Override
    public Integer findEventNotificationByUserId(Long id) {
        return findEventNotificationByUserId(id);
    }

    @Override
    public Integer findEventNotificationByUserIdAndEventId(Long id, Long eventId) {
        return findEventNotificationByUserIdAndEventId(id, eventId);
    }

    @Override
    public void createEventNotificationFromParams(Long userId, Long eventId) {
        notifRepo.createEventNotificationFromParams(userId, eventId);
    }

    @Override
    public void deleteEventNotificationByEventId(Long eventId) {
        notifRepo.deleteEventNotificationByEventId(eventId);
    }

    @Override
    public void deleteNotificationByUserId(Long userId) {
        notifRepo.deleteNotificationByUserId(userId);
    }
}
