package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Notification;
import com.inthergroup.internship.models.ResponsibilityCard;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dummy on 04/07/2017.
 */

public interface ResponsibilityCardsService {

     List<ResponsibilityCard> findAllResponsibilityCards();

     List<ResponsibilityCard> findAllAvailableResponsibilityCards();

     ResponsibilityCard create(ResponsibilityCard rc);

     ResponsibilityCard edit(ResponsibilityCard rc);

     ResponsibilityCard findById(Long id);

     void deleteById(Long id);

     //Integer findResponsibilityCardIdByUserId(Long id);

     List<Object[]> findCurrentResponsibilityCardsByUserId(Long id);

     void updateResponsibilityCardByCardId(Long cardId);

     List<ResponsibilityCard> findResponsibilityCardsByUserId(Long id);
}
