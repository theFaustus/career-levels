package com.inthergroup.internship.services;

import com.inthergroup.internship.models.Assignment;
import com.inthergroup.internship.repositories.AssignmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Faust on 7/5/2017.
 */
@Service
@Primary
public class AssignmentServiceJpaImpl implements AssignmentService{

    @Autowired
    private AssignmentRepository assignmentRepository;

    @Override
    public List<Object[]> findAllAssignments() {
        return assignmentRepository.findAllAssignments();
    }

    @Override
    public Assignment createAssignment(Assignment a) {
        return assignmentRepository.save(a);
    }

    @Override
    public Integer findAssignmentIdByUserId(Long id) {
        return null;
    }

    @Override
    public Assignment edit(Assignment n) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }

    @Override
    public void deleteAssignmentsByUserId(Long userId) {
        assignmentRepository.deleteAssignmentsByUserId(userId);
    }
}
