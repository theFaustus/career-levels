package com.inthergroup.internship.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.inthergroup.internship.models.TodoType;

@Repository
public interface TodoTypeRepository extends JpaRepository<TodoType, Long> {
    
    /**
     * Returns a list of all instances of TodoType entity ordered by Id ascending.
     */
    public List<TodoType> findAllByOrderByIdAsc();

    /**
     * Returns list of arrays that consist of points, mandatory and todo type names
     * for specific career level.
     * 1st element in array - points.
     * 2nd element in array - name of todo type.
     * @param id Career level id.
     */
    @Query("select ct.points, ct.mandatory, tt.name " +
            "from CareerLevelTodo ct, TodoType tt " +
            "where ct.primaryKey.todoType.id = tt.id " +
            "and ct.primaryKey.careerLevel.id = ?1")
    public List<Object[]> findTodosFromLevel(Long id);

    /**
     * Returns list of arrays that consist of points, mandatory and just mandatory todo type names
     * for specific career level.
     * 1st element in array - points.
     * 2nd element in array - name of todo type.
     * @param id Career level id.
     */
    @Query("select ct.points, ct.mandatory, tt.name " +
            "from CareerLevelTodo ct, TodoType tt " +
            "where ct.primaryKey.todoType.id = tt.id " +
            "and ct.mandatory = 1" +
            "and ct.primaryKey.careerLevel.id = ?1")
    public List<Object[]> findMandatoryTodosFromLevel(Long id);

    /**
     * Returns list of arrays that consist of points, mandatory and just optional todo type names
     * for specific career level.
     * 1st element in array - points.
     * 2nd element in array - name of todo type.
     * @param id Career level id.
     */
    @Query("select ct.points, ct.mandatory, tt.name " +
            "from CareerLevelTodo ct, TodoType tt " +
            "where ct.primaryKey.todoType.id = tt.id " +
            "and ct.mandatory = 0" +
            "and ct.primaryKey.careerLevel.id = ?1")
    public List<Object[]> findOptionalTodosFromLevel(Long id);

    /**
     * Returns a list of all ToDoTypes names that correspond to a specific career level.
     *
     * @param id
     */
    @Query("select new TodoType(tt.id, tt.name) " +
            "from CareerLevelTodo ct, TodoType tt " +
            "where ct.primaryKey.todoType.id = tt.id " +
            "and ct.primaryKey.careerLevel.id = ?1")
    public List<TodoType> findTodoTypesFromLevel(Long id);
}
