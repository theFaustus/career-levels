package com.inthergroup.internship.repositories;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Faust on 7/3/2017.
 */
@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    @Query("select u.id, u.firstName, u.lastName, u.careerLevel.id from Notification p, User u " +
            "where p.user.id = u.id " +
            "and p.type = 1")
    List<Object[]> findAllPromoteNotifications();

    @Query("select p.id from Notification p, User u " +
            "where p.user.id = u.id " +
            "and p.type = 1 " +
            "and p.user.id = :userId")
    Integer findPromoteNotificationByUserId(@Param("userId") Long id);

    @Query("select u.id, u.firstName, u.lastName, u.careerLevel.id, rc.type, rc.id from Notification p, User u, ResponsibilityCard rc " +
            "where p.user.id = u.id " +
            "and p.card.id = rc.id " +
            "and p.type = 2")
    List<Object[]> findAllRequestNotifications();

    @Query("select p.id from Notification p, User u " +
            "where p.user.id = u.id " +
            "and p.type = 2" +
            "and p.user.id = :userId")
    Integer findRequestNotificationByUserId(@Param("userId") Long id);

    @Query("select p.id from Notification p, User u " +
            "where p.user.id = u.id " +
            "and p.type = 2" +
            "and p.user.id = :userId " +
            "and p.card.id = :reqCardId")
    Integer findRequestNotificationByUserIdAndReqCardId(@Param("userId") Long id, @Param("reqCardId") Long reqCardId);

    @Query(value = "insert into notification (user_id, type, req_card_id) values (?, 2, ?)", nativeQuery = true)
    @Transactional
    @Modifying
    void createRequestNotificationFromParams(Long userId, Long reqCardId);


    @Query(value = "insert into notification (user_id, type, event_id) values (?, 3, ?)", nativeQuery = true)
    @Transactional
    @Modifying
    void createEventNotificationFromParams(Long userId, Long eventId);

    @Query("select p, u, e from Notification p, User u, Event e " +
            "where p.user.id = u.id " +
            "and p.event.id = e.id " +
            "and p.type = 3 ")
    List<Object[]> findAllEventNotifications();

    @Query("select p.id from Notification p, User u " +
            "where p.user.id = u.id " +
            "and p.type = 3" +
            "and p.user.id = :userId ")
    Integer findEventNotificationByUserId(@Param("userId") Long id);

    @Query("select p.id from Notification p, User u " +
            "where p.user.id = u.id " +
            "and p.type = 3" +
            "and p.user.id = :userId " +
            "and p.event.id = :eventId ")
    Integer findEventNotificationByUserIdAndEventId(@Param("userId") Long id, @Param("eventId") Long eventId);


    @Query(value = "delete from notification where event_id = ?", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventNotificationByEventId(Long eventId);

    @Query(value = "delete from notification where user_id = ?", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteNotificationByUserId(Long userId);

}
