package com.inthergroup.internship.repositories;

import com.inthergroup.internship.models.Assignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Faust on 7/5/2017.
 */
@Repository
public interface AssignmentRepository extends JpaRepository<Assignment, Long>{

    /*
        1 - is for admin, change it whenever the admin`s id changes
     */

    @Query("select a.id, rc.imagePath, u.firstName, u.lastName, rc.type, rc.description, a.assignDate from Assignment a, User u, ResponsibilityCard rc " +
            "where a.user.id = u.id " +
            "and a.card.id = rc.id " +
            "and a.user.id <> 1")
    List<Object[]> findAllAssignments();


    @Query(value = "delete from assignments where user_id = :userId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteAssignmentsByUserId(@Param("userId") Long userId);
}
