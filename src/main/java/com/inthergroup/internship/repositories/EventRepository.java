package com.inthergroup.internship.repositories;

import com.inthergroup.internship.models.Event;
import com.inthergroup.internship.models.EventAttend;
import com.inthergroup.internship.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Faust on 7/6/2017.
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    @Query("select e from Event e " +
            "where e.date < current_timestamp " +
            "and e.active = 1 ")
    List<Event> findAllAvailableEvents();

    @Query("select e from Event e " +
            "where e.userAuthor.id = :userId")
    List<Event> findAllEventsCreatedByUserId(@Param("userId") Long userId);

    @Query("select e from Event e " +
            "where e.date < current_timestamp " +
            "and e.id = :eventId " +
            "and e.active = 1")
    Event findAvailableEventByEventId(@Param("eventId") Long eventId);

    @Query("select e.id, u, ec from Event e, User u, EventAttend ea, EventComment ec " +
            "where ea.attender.id = u.id " +
            "and ea.event.id = e.id " +
            "and ec.attender.id = u.id " +
            "and ec.event.id = ea.event.id")
    List<Object[]> findAllAttenders();

    @Query("select e.id, u, ec from Event e, User u, EventAttend ea, EventComment ec " +
            "where ea.attender.id = u.id " +
            "and ea.event.id = e.id " +
            "and ec.attender.id = u.id " +
            "and ec.event.id = ea.event.id")
    List<Object[]> findAllEventAttenders();

    Event save(Event event);

    @Query("select et from EventAttend et")
    List<EventAttend> findAllEventAttends();

    @Query("delete from EventAttend et where et.event.id = :eventId " +
            "and et.attender.id = :userId")
    @Transactional
    @Modifying
    void deleteEventAttendByEventIdAndByUserId(@Param("eventId") Long eventId, @Param("userId") Long userId);

    @Query("update EventComment et set et.text = :text where et.id = :commentId")
    @Transactional
    @Modifying
    void updateEventCommentByCommentId(@Param("commentId") Long commentId, @Param("text") String text);

    @Query("delete from EventComment et where et.event.id = :eventId " +
            "and et.attender.id = :userId")
    @Transactional
    @Modifying
    void deleteEventCommentByEventIdAndUserId(@Param("eventId") Long eventId, @Param("userId") Long userId);

    @Query(value = "insert into event_attend (event_id, user_id) values (?, ?)", nativeQuery = true)
    @Transactional
    @Modifying
    void createEventAttend(Long eventId, Long userId);

    @Query(value = "insert into event_comments (event_id, user_id, text) values (?, ?, ?)", nativeQuery = true)
    @Transactional
    @Modifying
    void createEventComment(Long eventId, Long userId, String text);

    @Query("select e from Event e, EventSeen es " +
            "where e.id = es.event.id " +
            "and es.attender.id = :userId ")
    List<Event> findEventsSeenByUserId(@Param("userId") Long id);

    @Query(value = "insert into event_seen (event_id, user_id) values (?, ?)", nativeQuery = true)
    @Transactional
    @Modifying
    void createEventSeen(Long eventId, Long userId);

    @Query("select et.id from EventAttend et " +
            "where et.event.id = :eventId " +
            "and et.attender.id = :userId ")
    Integer findEventAttendByUserIdAndEventId(@Param("userId") Long userId, @Param("eventId") Long eventId);

    @Query("select es.id from EventSeen es " +
            "where es.event.id = :eventId " +
            "and es.attender.id = :userId ")
    Integer findEventSeenByUserIdAndEventId(@Param("userId") Long userId, @Param("eventId") Long eventId);

    @Query("select es.id from EventComment es " +
            "where es.event.id = :eventId " +
            "and es.attender.id = :userId ")
    Integer findEventCommentByUserIdAndEventId(@Param("userId") Long userId, @Param("eventId") Long eventId);

    @Query("select e from Event e, EventAttend ea " +
            "where ea.attender.id <> ea.event.id " +
            "and e.id = ea.event.id " +
            "and ea.attender.id = :userId")
    List<Event> getAttendedEventByUserId(@Param("userId") Long userId);

    @Query(value = "delete from event_attend where user_id = :userId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventAttendByUserId(@Param("userId") Long userId);

    @Query(value = "delete from events where author_id = :userId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventByUserId(@Param("userId") Long userId);

    @Query(value = "delete from event_seen where user_id = :userId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventSeenByUserId(@Param("userId") Long userId);

    @Query(value = "delete from event_comments where user_id = :userId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventCommentByUserId(@Param("userId") Long userId);

    @Query(value = "delete from event_attend where event_id = :eventId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventAttendByEventId(@Param("eventId") Long eventId);

    @Query(value = "delete from event_seen where event_id = :eventId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventSeenByEventId(@Param("eventId") Long eventId);

    @Query(value = "delete from event_comments where event_id = :eventId", nativeQuery = true)
    @Transactional
    @Modifying
    void deleteEventCommentByEventId(@Param("eventId") Long eventId);


}
