package com.inthergroup.internship.repositories;

import com.inthergroup.internship.models.ResponsibilityCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dummy on 04/07/2017.
 */
@Repository
public interface ResponsibilityCardRepository extends JpaRepository<ResponsibilityCard, Long> {
    @Query("select new ResponsibilityCard(rc, u) from ResponsibilityCard rc, User u " +
            "where rc.user.id = u.id")
    List<ResponsibilityCard> findAllResponsibilityCards();

    @Query("select new ResponsibilityCard(rc, u) from ResponsibilityCard rc, User u " +
            "where rc.user.id = u.id " +
            "and u.id = 1")
    List<ResponsibilityCard> findAllAvailableResponsibilityCards();

    @Query("select rc from ResponsibilityCard rc " +
            "where rc.user.id = :userId")
    List<Object[]> findCurrentResponsibilityCardsByUserId(@Param("userId") Long id);


    @Query("select rc from ResponsibilityCard rc " +
            "where rc.user.id = :userId")
    List<ResponsibilityCard> findResponsibilityCardsByUserId(@Param("userId") Long id);

    @Query(value = "update responsibility_cards set user_id=(select u.id from users u where u.group_id = 2 limit 1 ) " +
            "where id = :cardId", nativeQuery = true)
    @Transactional
    @Modifying
    void updateResponsibilityCardUser(@Param("cardId") Long cardId);


}
